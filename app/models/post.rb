class Post < ApplicationRecord
  mount_uploader :picture, PictureUploader

  belongs_to :user
  counter_culture :user
  belongs_to :shop
  counter_culture :shop

  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :liked_users, through: :likes, source: :user

  validates :title, presence: true, length: { maximum: 30 }
  validates :description, presence: true
  validates :recomend, length: { maximum: 30 }

  scope :timeline, -> current_user {
    where(user_id: current_user.following.ids).
      order(updated_at: :desc)
  }
end
