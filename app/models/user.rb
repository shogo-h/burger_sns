class User < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :liked_posts, through: :likes, source: :post
  has_many :bookmarks, dependent: :destroy
  has_many :bookmarked_shops, through: :bookmarks, source: :shop
  has_many :active_relationships,
           class_name: "Relationship",
           foreign_key: "follower_id",
           dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :passive_relationships,
           class_name: "Relationship",
           foreign_key: "followed_id",
           dependent: :destroy
  has_many :followers, through: :passive_relationships, source: :follower

  validates :username, presence: true
  validates :introduction, length: { maximum: 300 }

  scope :new_users, -> { includes(:posts).order(created_at: :desc) }

  def favorite_shop_name
    if favorite.present?
      Shop.find(favorite).name
    else
      '-'
    end
  end

  def get_likes
    posts.sum { |post| post.likes_count }
  end

  def follow(other_user)
    following << other_user
  end

  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  def following?(other_user)
    following.include?(other_user)
  end

  def self.ransackable_attributes(auth_object = nil)
    %w(username introduction)
  end

  def self.ransackable_associations(auth_object = nil)
    []
  end
end
