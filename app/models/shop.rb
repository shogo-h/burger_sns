class Shop < ApplicationRecord
  mount_uploader :picture, PictureUploader

  has_many :posts, dependent: :destroy
  has_many :bookmarks, dependent: :destroy
  has_many :bookmarked_users, through: :bookmarks, source: :user

  validates :name, presence: true
  validates :station, presence: true, length: { maximum: 30 }

  scope :newest, -> { order(created_at: :desc) }

  def bookmarks_count
    bookmarked_users.count
  end

  def self.ransackable_attributes(auth_object = nil)
    %w(name address station)
  end

  def self.ransackable_associations(auth_object = nil)
    []
  end
end
