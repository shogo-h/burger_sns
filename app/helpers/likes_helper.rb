module LikesHelper
  def already_like?(post)
    Like.find_by(user: current_user, post: post).present? if user_signed_in?
  end
end
