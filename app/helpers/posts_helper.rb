module PostsHelper
  def display_post_image(post, resize)
    if post.picture?
      image_tag(post.picture.url, class: "#{resize}", alt: "#{post.title}の写真")
    end
  end

  def shorten_description(post)
    if post.description.length >= 100
      "#{post.description[0...100]}…"
    else
      post.description
    end
  end

  def post_time(time)
    "#{time_ago_in_words(time)}　#{simple_time(time)}"
  end

  def delete_comment(post, comment)
    if comment.user == current_user
      link_to(
        "削除する",
        post_comment_path(post, comment),
        method: :delete,
        data: { confirm: "コメントを削除します。よろしいですか？" },
        class: 'text-danger'
      )
    end
  end
end
