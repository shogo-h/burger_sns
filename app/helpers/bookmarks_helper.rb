module BookmarksHelper
  def already_bookmark?(shop)
    Bookmark.find_by(user: current_user, shop: shop).present? if user_signed_in?
  end
end
