module ShopsHelper
  def display_image(shop)
    if shop.picture?
      image_tag(shop.picture.url, class: 'card-img-top rounded', alt: "#{shop.name}")
    else
      image_tag("/assets/noimage.jpg", class: 'card-img-top rounded', alt: "#{shop.name}")
    end
  end
end
