module ApplicationHelper
  def full_title(page_title = '')
    base_title = "Burger Mania!"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def simple_time(time)
    time.strftime('%Y/%m/%d %H:%M')
  end
end
