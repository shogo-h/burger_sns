module UsersHelper
  def shorten_introduction(user)
    if user.introduction.present?
      "#{user.introduction[0...38]}…"
    else
      "-"
    end
  end

  def display_user_image(user, resize)
    if user.avatar?
      image_tag(user.avatar.url, class: "rounded-circle #{resize} border")
    else
      image_tag('/assets/no_avatar.jpg', class: "rounded-circle #{resize} border")
    end
  end

  def current_user?(user)
    user == current_user
  end
end
