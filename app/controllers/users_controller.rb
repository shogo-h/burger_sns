class UsersController < ApplicationController
  before_action :set_user, only: [:show, :bookmark, :following, :followers]
  before_action :set_posts, only: [:show, :bookmark, :following, :followers]
  before_action :set_favorite_shop, only: [:show, :bookmark, :following, :followers]

  def index
    @q = User.all.ransack(params[:q])
    if params[:sort] == "popular"
      @users = User.
        includes(:posts).
        joins(posts: :likes).
        group("users.id").
        order(Arel.sql("count(likes.id) desc")).
        page(params[:page])
    else
      @users = @q.result(distinct: true).new_users.page(params[:page])
    end
  end

  def show
  end

  def bookmark
    @shops = @user.bookmarked_shops.page(params[:page])
  end

  def following
    @users = @user.following.includes(:posts).page(params[:page])
  end

  def followers
    @users = @user.followers.includes(:posts).page(params[:page])
  end

  private

  def set_user
    @user = User.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end

  def set_posts
    @posts = @user.posts.includes(:shop).page(params[:page])
  end

  def set_favorite_shop
    @favorite_shop = Shop.find(@user.favorite) if @user.favorite.present?
  end
end
