class PagesController < ApplicationController
  def index
    @q = Shop.ransack(params[:q])
    @popular_users = User.
      includes(:posts).
      joins(posts: :likes).
      group("users.id").
      order(Arel.sql("count(likes.id) desc")).
      limit(4)
    @new_shops = Shop.newest.limit(4)
    @popular_shops = Shop.
      joins(:bookmarks).
      group("shops.id").
      order(Arel.sql("count(bookmarks.id) desc")).
      limit(4)
  end
end
