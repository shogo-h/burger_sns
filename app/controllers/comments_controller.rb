class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @post = Post.find(params[:post_id])
    @comment = current_user.comments.build(comment_params)
    @comment.post_id = @post.id
    @comments = Post.find(params[:post_id]).comments.all
    if @comment.save
      redirect_to post_path(@comment.post_id), notice: "コメントしました。"
    else
      render "posts/show"
    end
  end

  def destroy
    @comment = current_user.comments.find(params[:id])
    @comment.destroy
    redirect_to post_path(@comment.post_id), notice: "コメントを削除しました。"
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :post_id)
  end
end
