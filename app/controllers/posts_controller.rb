class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy, :timeline]
  before_action :set_post, only: [:edit, :update, :destroy]
  before_action :set_shop, only: [:index, :new, :create]

  def index
    @posts = @shop.posts.includes(:user).page(params[:page])
  end

  def show
    @post = Post.find(params[:id])
    @comments = @post.comments.includes(:user).all
    @comment = Comment.new
    @shop = @post.shop
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.build(post_params)
    @post.shop_id = @shop.id
    if @post.save
      redirect_to post_path(@post), notice: "レビュー「#{@post.title}」を投稿しました。"
    else
      render :new
    end
  end

  def edit
    @shop = @post.shop
  end

  def update
    if @post.update(post_params)
      redirect_to post_path(@post), notice: "レビュー「#{@post.title}」を更新しました。"
    else
      @shop = @post.shop
      render :edit
    end
  end

  def destroy
    @post.destroy
    redirect_to root_path, notice: "レビュー「#{@post.title}」を削除しました。"
  end

  def timeline
    @q = User.ransack(params[:q])
    @posts = Post.includes(:user, :shop).timeline(current_user).page(params[:page])
  end

  private

  def post_params
    params.require(:post).permit(:title,
                                 :description,
                                 :recomend,
                                 :picture,
                                 :remove_picture,
                                 :picture_cache,
                                 :shop_id)
  end

  def set_post
    @post = current_user.posts.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end

  def set_shop
    @shop = Shop.find(params[:shop_id])
  end
end
