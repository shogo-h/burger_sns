class ShopsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :admin_user, only: [:destroy]
  before_action :set_shop, only: [:show, :edit, :update, :destroy]

  def index
    @q = Shop.ransack(params[:q])
    if params[:sort] == "popular"
      @shops = Shop.
        joins(:bookmarks).
        group("shops.id").
        order(Arel.sql("count(bookmarks.id) desc")).
        page(params[:page])
    else
      @shops = @q.result(distinct: true).newest.page(params[:page])
    end
  end

  def show
    @posts = @shop.posts.includes(:user).order(updated_at: :desc).limit(4)
  end

  def new
    @shop = Shop.new
  end

  def create
    @shop = Shop.new(shop_params)

    if @shop.save
      redirect_to @shop, notice: "お店「#{@shop.name}」を登録しました。"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @shop.update(shop_params)
      redirect_to @shop, notice: "お店「#{@shop.name}」を更新しました。"
    else
      render :edit
    end
  end

  def destroy
    @shop.destroy
    redirect_to shops_path, notice: "お店「#{@shop.name}」を削除しました。"
  end

  private

  def admin_user
    unless current_user.admin?
      flash[:alert] = "お店を削除することはできません。"
      redirect_back(fallback_location: root_path)
    end
  end

  def shop_params
    params.require(:shop).permit(:name,
                                 :menu,
                                 :hour,
                                 :holiday,
                                 :address,
                                 :phone,
                                 :url,
                                 :picture,
                                 :remove_picture,
                                 :picture_cache,
                                 :station)
  end

  def set_shop
    @shop = Shop.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end
end
