class BookmarksController < ApplicationController
  before_action :authenticate_user!

  def create
    @shop = Shop.find(params[:shop_id])
    bookmark = current_user.bookmarks.build(shop_id: params[:shop_id])
    if bookmark.save
      respond_to do |format|
        format.html { redirect_to shop_path(@shop) }
        format.js
      end
    else
      render 'shop/show'
    end
  end

  def destroy
    @shop = Shop.find(params[:shop_id])
    bookmark = Bookmark.find_by(user: current_user, shop: @shop)
    if bookmark.destroy
      respond_to do |format|
        format.html { redirect_to shop_path(@shop) }
        format.js
      end
    else
      render 'shop/show'
    end
  end
end
