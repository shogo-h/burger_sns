class LikesController < ApplicationController
  before_action :authenticate_user!

  def create
    @post = Post.find(params[:post_id])
    like = current_user.likes.build(post_id: params[:post_id])
    like.save
    respond_to do |format|
      format.html { redirect_to post_path(@post) }
      format.js
    end
  end

  def destroy
    @post = Post.find(params[:post_id])
    like = Like.find_by(user: current_user, post: @post)
    like.destroy
    respond_to do |format|
      format.html { redirect_to post_path(@post) }
      format.js
    end
  end
end
