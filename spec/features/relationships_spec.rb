require 'rails_helper'

RSpec.feature "Relationships", type: :feature do
  feature 'フォロー・アンフォロー' do
    given(:user) { create(:user) }
    given(:other_user) { create(:user) }

    background do
      sign_in user
    end

    scenario 'フォローする' do
      visit user_path(other_user)

      expect do
        expect(page).to have_selector ".follower-t p", text: '0'
        expect(page).to have_css ".border-primary"
        expect(page).not_to have_css ".btn-primary"

        find("input[value='フォロー']").click

        expect(page).to have_selector ".follower-t p", text: '1'
        expect(page).to have_css ".btn-primary"
        expect(page).not_to have_css ".border-primary"
      end.to change { user.following.count }.by(1)
    end

    scenario 'アンフォローする' do
      Relationship.create(follower_id: user.id, followed_id: other_user.id)
      visit user_path(other_user)

      expect do
        expect(page).to have_selector ".follower-t p", text: '1'
        expect(page).to have_css ".btn-primary"
        expect(page).not_to have_css ".border-primary"

        find("input[value='アンフォロー']").click

        expect(page).to have_selector ".follower-t p", text: '0'
        expect(page).to have_css ".border-primary"
        expect(page).not_to have_css ".btn-primary"
      end.to change { user.following.count }.by(-1)
    end
  end
end
