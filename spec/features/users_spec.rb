require 'rails_helper'

RSpec.feature "Users", type: :feature do
  feature '新規登録ページ' do
    background do
      visit new_user_registration_path
    end

    scenario '新規登録ページを表示する' do
      expect(page).to have_current_path new_user_registration_path
      expect(page).to have_title '新規登録 | Burger Mania!'
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_selector '.current', text: '新規登録'
      end
      expect(page).to have_selector 'h2', text: '新規登録'
      expect(page).to have_selector 'label', text: 'アカウント名'
      expect(page).to have_selector 'input#user_username'
      expect(page).to have_selector 'label', text: 'メールアドレス'
      expect(page).to have_selector 'input#user_email'
      expect(page).to have_selector 'label', text: 'パスワード'
      expect(page).to have_selector 'input#user_password'
      expect(page).to have_selector 'label', text: 'パスワード確認'
      expect(page).to have_selector 'input#user_password_confirmation'
      expect(page).to have_button '登録する'
      within '.col-md-8' do
        expect(page).to have_link 'ログイン'
      end
    end

    scenario '新規登録を行う' do
      expect do
        fill_in 'アカウント名', with: 'taro'
        fill_in 'メールアドレス', with: 'test@example.com'
        fill_in 'パスワード', with: 'password'
        fill_in 'パスワード確認', with: 'password'
        click_button '登録する'

        user = User.last

        expect(page).to have_current_path user_path(user)
        expect(page).to have_selector '.alert-success', text: 'アカウント登録を受け付けました。'
      end.to change(User, :count).by(1)
    end

    scenario 'ログインをクリックして、ログインページに遷移する' do
      within '.col-md-8' do
        click_link 'ログイン'
      end

      expect(page).to have_current_path new_user_session_path
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end
  end

  feature 'ログインページ' do
    background do
      visit new_user_session_path
    end

    scenario 'ログインページを表示する' do
      expect(page).to have_current_path new_user_session_path
      expect(page).to have_title 'ログイン | Burger Mania!'
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_selector '.current', text: 'ログイン'
      end
      expect(page).to have_selector 'h2', text: 'ログイン'
      expect(page).to have_selector 'label', text: 'メールアドレス'
      expect(page).to have_selector 'input#user_email'
      expect(page).to have_selector 'label', text: 'パスワード'
      expect(page).to have_selector 'input#user_password'
      expect(page).to have_button 'ログイン'
      expect(page).to have_field('ログインしたままにする')
      within '.col-md-8' do
        expect(page).to have_link '新規登録'
      end
    end

    scenario '新規登録をクリックして、新規登録ページに遷移する' do
      within '.col-md-8' do
        click_link '新規登録'
      end

      expect(page).to have_current_path new_user_registration_path
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'ログインして、ログアウトする' do
      user = create(:user)

      fill_in 'メールアドレス', with: user.email
      fill_in 'パスワード', with: user.password
      click_button 'ログイン'

      expect(page).to have_current_path root_path
      expect(page).to have_selector '.alert-success', text: 'ログインしました。'
      click_link 'ログアウト'

      expect(page).to have_current_path root_path
      expect(page).to have_selector '.alert-success', text: 'ログアウトしました。'
    end
  end

  feature 'ユーザー一覧（ユーザーを探す）ページ' do
    scenario 'ユーザー一覧が表示される' do
      shop = create(:shop, name: "立石バーガー")
      users = create_list(:user, 2)
      one_user = create(:user, favorite: shop.id)
      post = create(:post, user_id: one_user.id)
      create(:like, post_id: post.id, user_id: users[1].id)

      visit users_path

      expect(page).to have_current_path users_path
      expect(page).to have_title 'ユーザーを探す | Burger Mania!'
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_selector '.current', text: 'ユーザーを探す'
      end
      expect(page).to have_selector 'h2', text: 'ユーザーを探す'
      expect(page).to have_selector 'input#q_username_or_introduction_cont'
      expect(page).to have_selector 'input[value="検索"]'
      expect(page).to have_content '3件のユーザーが表示されています'
      expect(all('.card').size).to eq(3)
      within "#user-#{one_user.id}" do
        expect(page).to have_selector "h5", text: '太郎'
        expect(page).to have_selector "p", text: 'レビュー：1'
        expect(page).to have_selector "p", text: 'いいね：1'
        expect(page).to have_selector "p", text: 'お気に入りのお店：立石バーガー'
      end
      within "#user-#{users[0].id}" do
        expect(page).to have_selector "h5", text: '太郎'
        expect(page).to have_selector "p", text: 'レビュー：0'
        expect(page).to have_selector "p", text: 'いいね：0'
        expect(page).to have_selector "p", text: 'お気に入りのお店：-'
      end
    end

    scenario 'ページネーションで遷移する' do
      create_list(:user, 21)

      visit users_path

      expect(page).to have_selector 'ul.pagination'
      expect(all('ul.pagination').size).to eq(2)
      expect(page).to have_selector 'li.active', text: '1'
      expect(page).to have_selector 'li.page-item', text: '2'
      expect(page).to have_selector 'li.page-item', text: '次 ›'
      expect(page).to have_selector 'li.page-item', text: '最後 »'
      expect(page).to have_content '全21件中 1 - 20件のユーザーが表示されています'
      expect(all('div.card').size).to eq(20)
      click_link '2', match: :first

      expect(page).to have_current_path '/users?page=2'
      expect(page).to have_selector 'li.active', text: '2'
      expect(page).to have_content '全21件中 21 - 21件のユーザーが表示されています'
      expect(all('div.card').size).to eq(1)
      click_link '1', match: :first

      expect(page).to have_current_path '/users'
      click_link '次', match: :first

      expect(page).to have_current_path '/users?page=2'
      click_link '前', match: :first

      expect(page).to have_current_path '/users'
      click_link '最後', match: :first

      expect(page).to have_current_path '/users?page=2'
      click_link '最初', match: :first

      expect(page).to have_current_path '/users'
    end

    scenario 'ユーザーを選択し、詳細ページに遷移する' do
      user = create(:user)
      visit users_path

      find("#user-#{user.id}").click

      expect(page).to have_current_path user_path(user)
    end

    scenario 'ユーザーを検索する' do
      create_list(:user, 3)
      one_user = create(:user, username: "テストさん", introduction: "特にチーズバーガーが好きです。")

      visit users_path

      fill_in 'q_username_or_introduction_cont', with: 'テストさん'
      click_button '検索'

      expect(page).to have_content '1件のユーザーが表示されています'
      expect(page).to have_selector "#user-#{one_user.id} h5", text: 'テストさん'

      fill_in 'q_username_or_introduction_cont', with: 'チーズバーガー'
      click_button '検索'

      expect(page).to have_content '1件のユーザーが表示されています'
      expect(page).to have_selector "#user-#{one_user.id} .introduction", text: '特にチーズバーガーが好きです。'
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      visit users_path

      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end
  end

  feature 'ユーザー詳細ページ' do
    include ActionView::Helpers::DateHelper
    include ApplicationHelper
    include PostsHelper

    given!(:shop) { create(:shop) }
    given!(:user) { create(:user, favorite: shop.id) }
    given!(:users) { create_list(:user, 2) }
    given!(:posts) { create_list(:post, 4, user_id: user.id) }

    background do
      create(:like, post_id: posts[0].id)
      create_list(:bookmark, 3, user_id: user.id)
      Relationship.create(follower: user, followed: users[0])
      Relationship.create(follower: user, followed: users[1])
      Relationship.create(follower: users[0], followed: user)

      visit user_path(user)
    end

    scenario 'ユーザー詳細ページを表示する' do
      expect(page).to have_current_path user_path(user)
      expect(page).to have_title '太郎さんのプロフィール | Burger Mania!'
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'ユーザーを探す'
        expect(page).to have_selector '.current', text: '太郎さんのプロフィール'
      end
      within '.col-md-4' do
        expect(page).to have_selector 'h2', text: '太郎'
        expect(page).to have_selector 'p', text: 'いいね獲得数：1'
        expect(page).to have_content 'お気に入りのお店'
        within "#shop-#{shop.id}" do
          expect(page).to have_selector 'h5', text: 'ハンバーガー屋'
          expect(page).to have_selector 'p', text: '渋谷駅'
          expect(page).to have_selector 'p', text: '東京都渋谷区宇田川町1-1'
          expect(page).to have_selector '.fa-star'
          expect(page).to have_selector '.card-footer .bookmark-t', text: '0'
          expect(page).to have_selector '.fa-comment-dots'
          expect(page).to have_selector '.card-footer .post-t', text: '0'
        end
      end

      within '.profile-nav' do
        expect(page).to have_selector '.post-t p', text: 'レビュー'
        expect(page).to have_selector '.post-t p', text: '4'
        expect(page).to have_selector '.bookmark-t p', text: '行きたいお店'
        expect(page).to have_selector '.bookmark-t p', text: '3'
        expect(page).to have_selector '.followed-t p', text: 'フォロー'
        expect(page).to have_selector '.followed-t p', text: '2'
        expect(page).to have_selector '.follower-t p', text: 'フォロワー'
        expect(page).to have_selector '.follower-t p', text: '1'
      end

      within '.col-md-8' do
        expect(page).to have_selector 'h2', text: 'レビュー'
        expect(page).to have_content '4件のレビューが表示されています'
        expect(all('.post-card-t').size).to eq(4)
        within "#post-#{posts[0].id}" do
          expect(page).to have_selector "img"
          expect(page).to have_selector "p b", text: '太郎'
          expect(page).to have_selector "p", text: "お店：#{posts[0].shop.name}"
          expect(page).to have_selector "div h3", text: posts[0].title
          expect(page).to have_selector "div p", text: posts[0].description
          expect(page).to have_selector "div p", text: "おすすめメニュー：#{posts[0].recomend}"
          expect(page).to have_selector ".comment-t .fa-comments"
          expect(page).to have_selector ".comment-t", text: "0"
          expect(page).to have_selector ".like-t .fa-heart"
          expect(page).to have_selector ".like-t", text: "1"
          expect(page).to have_selector "p.col-7", text: post_time(posts[0].updated_at)
        end
      end
    end

    scenario 'お気に入りのお店をクリックして、詳細ページに遷移する' do
      find("#shop-#{shop.id}").click

      expect(page).to have_current_path shop_path(shop)
    end

    scenario 'レビューをクリックして、詳細ページに遷移する' do
      find("#post-#{posts[0].id}").click

      expect(page).to have_current_path post_path(posts[0])
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、ユーザー一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'ユーザーを探す'
      end

      expect(page).to have_current_path users_path
    end
  end

  feature '行きたいお店ページ' do
    given(:user) { create(:user) }

    background do
      create_list(:bookmark, 3, user_id: user.id)
      @shop = user.bookmarked_shops.last

      visit bookmark_user_path(user)
    end

    scenario '行きたいお店ページを表示する' do
      expect(page).to have_current_path bookmark_user_path(user)
      expect(page).to have_title "太郎さんの行きたいお店 | Burger Mania!"
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'ユーザーを探す'
        expect(page).to have_link "太郎さんのプロフィール"
        expect(page).to have_selector '.current', text: '行きたいお店'
      end

      within '.col-md-8' do
        expect(page).to have_selector 'h2', text: '行きたいお店'
        expect(page).to have_content '3件のお店が表示されています'
        expect(all('.card').size).to eq(3)
        within "#shop-#{@shop.id}" do
          expect(page).to have_selector 'h5', text: 'ハンバーガー屋'
          expect(page).to have_selector 'p', text: '渋谷駅'
          expect(page).to have_selector 'p', text: '東京都渋谷区宇田川町1-1'
          expect(page).to have_selector '.fa-star'
          expect(page).to have_selector '.card-footer .bookmark-t', text: '1'
          expect(page).to have_selector '.fa-comment-dots'
          expect(page).to have_selector '.card-footer .post-t', text: '0'
        end
      end
    end

    scenario '行きたいお店をクリックして、詳細ページに遷移する' do
      find("#shop-#{@shop.id}").click

      expect(page).to have_current_path shop_path(@shop)
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、ユーザー一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'ユーザーを探す'
      end

      expect(page).to have_current_path users_path
    end

    scenario 'パンくずリストをクリックして、ユーザー詳細ページに戻る' do
      within '.breadcrumbs' do
        click_link "太郎さんのプロフィール"
      end

      expect(page).to have_current_path user_path(user)
    end
  end

  feature 'フォローページ' do
    given(:user) { create(:user) }
    given(:users) { create_list(:user, 2) }

    background do
      Relationship.create(follower: user, followed: users[0])
      Relationship.create(follower: user, followed: users[1])

      visit following_user_path(user)
    end

    scenario 'フォローページを表示する' do
      expect(page).to have_current_path following_user_path(user)
      expect(page).to have_title "太郎さんがフォローしているユーザー | Burger Mania!"
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'ユーザーを探す'
        expect(page).to have_link "太郎さんのプロフィール"
        expect(page).to have_selector '.current', text: 'フォロー'
      end

      within '.col-md-8' do
        expect(page).to have_selector 'h2', text: 'フォロー'
        expect(page).to have_content '2件のユーザーが表示されています'
        expect(all('.card').size).to eq(2)
        within "#user-#{users[0].id}" do
          expect(page).to have_selector "h5", text: "太郎"
          expect(page).to have_selector "p", text: 'レビュー：0'
          expect(page).to have_selector "p", text: 'いいね：0'
          expect(page).to have_selector "p", text: 'お気に入りのお店：-'
        end
      end
    end

    scenario 'ユーザーをクリックして、詳細ページに遷移する' do
      find("#user-#{users[0].id}").click

      expect(page).to have_current_path user_path(users[0])
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、ユーザー一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'ユーザーを探す'
      end

      expect(page).to have_current_path users_path
    end

    scenario 'パンくずリストをクリックして、ユーザー詳細ページに戻る' do
      within '.breadcrumbs' do
        click_link "太郎さんのプロフィール"
      end

      expect(page).to have_current_path user_path(user)
    end
  end

  feature 'フォロワーページ' do
    given(:user) { create(:user) }
    given(:users) { create_list(:user, 2) }

    background do
      Relationship.create(follower: users[0], followed: user)
      Relationship.create(follower: users[1], followed: user)

      visit followers_user_path(user)
    end

    scenario 'フォロワーページを表示する' do
      expect(page).to have_current_path followers_user_path(user)
      expect(page).to have_title "太郎さんをフォローしているユーザー | Burger Mania!"
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'ユーザーを探す'
        expect(page).to have_link "太郎さんのプロフィール"
        expect(page).to have_selector '.current', text: 'フォロワー'
      end

      within '.col-md-8' do
        expect(page).to have_selector 'h2', text: 'フォロワー'
        expect(page).to have_content '2件のユーザーが表示されています'
        expect(all('.card').size).to eq(2)
        within "#user-#{users[0].id}" do
          expect(page).to have_selector "h5", text: "太郎"
          expect(page).to have_selector "p", text: 'レビュー：0'
          expect(page).to have_selector "p", text: 'いいね：0'
          expect(page).to have_selector "p", text: 'お気に入りのお店：-'
        end
      end
    end

    scenario 'ユーザーをクリックして、詳細ページに遷移する' do
      find("#user-#{users[0].id}").click

      expect(page).to have_current_path user_path(users[0])
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、ユーザー一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'ユーザーを探す'
      end

      expect(page).to have_current_path users_path
    end

    scenario 'パンくずリストをクリックして、ユーザー詳細ページに戻る' do
      within '.breadcrumbs' do
        click_link "太郎さんのプロフィール"
      end

      expect(page).to have_current_path user_path(user)
    end
  end

  feature 'ユーザー編集ページ' do
    given(:shop) { create(:shop) }
    given(:user) { create(:user, email: "test@example.com", favorite: shop.id) }

    background do
      sign_in user
      visit edit_user_registration_path
    end

    scenario 'ユーザー編集ページを表示する' do
      expect(page).to have_current_path edit_user_registration_path
      expect(page).to have_title 'ユーザー編集 | Burger Mania!'

      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'ユーザーを探す'
        expect(page).to have_link '太郎さんのプロフィール'
        expect(page).to have_selector '.current', text: 'ユーザー編集'
      end

      expect(page).to have_selector 'h2', text: 'ユーザー編集'
      expect(page).to have_field 'アカウント名', with: '太郎'
      expect(page).to have_field 'メールアドレス', with: 'test@example.com'
      expect(page).to have_selector 'label', text: 'プロフィール画像'
      expect(page).to have_selector '.form-group img'
      expect(page).to have_selector 'input#user_avatar'
      expect(page).to have_selector 'input#user_avatar_cache', visible: false
      expect(page).to have_field '自己紹介', with: 'テスト'
      expect(page).to have_select('お気に入りのお店', selected: 'ハンバーガー屋')
      expect(page).to have_selector 'label', text: 'パスワード'
      expect(page).to have_selector 'input#user_password'
      expect(page).to have_selector 'label', text: 'パスワード確認'
      expect(page).to have_selector 'input#user_password_confirmation'
      expect(page).to have_selector 'label', text: '現在のパスワード'
      expect(page).to have_selector 'input#user_current_password'
      expect(page).to have_button '更新する'
      expect(page).to have_selector 'h3', text: 'アカウントを削除する'
      expect(page).to have_button '削除する'
      expect(page).to have_link '戻る'
    end

    scenario 'ユーザー情報を更新する' do
      create(:shop, name: "チーズバーガー屋")

      visit current_path

      fill_in 'アカウント名', with: 'テスト太郎'
      fill_in 'メールアドレス', with: 'rspec@example.com'
      attach_file 'プロフィール画像', "#{Rails.root}/spec/factories/test.jpg"
      fill_in '自己紹介', with: 'テスト楽しいな！'
      select 'チーズバーガー屋', from: 'お気に入りのお店'
      fill_in 'パスワード', with: 'testtest'
      fill_in 'パスワード確認', with: 'testtest'
      fill_in '現在のパスワード', with: 'password'
      click_button '更新する'

      expect(page).to have_current_path user_path(user)
      expect(page).to have_selector '.alert-success', text: 'アカウントが更新されました。'
      click_on "ユーザー編集"

      expect(page).to have_current_path edit_user_registration_path

      expect(page).to have_field 'アカウント名', with: 'テスト太郎'
      expect(page).to have_field 'メールアドレス', with: 'rspec@example.com'
      expect(page).to have_field '自己紹介', with: 'テスト楽しいな！'
      expect(page).to have_css("img[src*='test.jpg']")
      expect(page).to have_select('お気に入りのお店', selected: 'チーズバーガー屋')

      fill_in '現在のパスワード', with: 'testtest'
      click_button '更新する'

      expect(page).to have_current_path user_path(user)
      expect(page).to have_selector '.alert-success', text: 'アカウントが更新されました。'
    end

    scenario 'ユーザーを削除する', js: true do
      expect do
        click_button '削除する'
        page.driver.browser.switch_to.alert.accept

        expect(page).to have_current_path root_path
        expect(page).to have_selector '.alert-success', text: 'アカウントを削除しました。またのご利用をお待ちしております。'
      end.to change(User, :count).by(-1)
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、ユーザー一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'ユーザーを探す'
      end

      expect(page).to have_current_path users_path
    end

    scenario 'パンくずリストをクリックして、ユーザー詳細ページに戻る' do
      within '.breadcrumbs' do
        click_link "太郎さんのプロフィール"
      end

      expect(page).to have_current_path user_path(user)
    end
  end
end
