require 'rails_helper'

RSpec.feature "Pages", type: :feature do
  feature 'トップページ' do
    given!(:users) { create_list(:user, 4, :with_post) }
    given!(:shops) { create_list(:shop, 4, :with_bookmark) }

    background do
      users.map { |user| create(:like, post: user.posts.first) }
      visit root_path
    end

    scenario 'トップページを表示する' do
      expect(page).to have_selector '.navbar-brand h1', text: 'Burger Mania!'
      expect(page).to have_selector '.navbar-brand img'
      expect(page).to have_link 'Burger Mania!', href: root_path
      expect(page).to have_link 'お店を探す', href: shops_path
      expect(page).to have_link 'ユーザーを探す', href: users_path
      expect(page).to have_link '新規登録', href: new_user_registration_path
      expect(page).to have_link 'ログイン', href: new_user_session_path

      expect(page).to have_selector 'h2', text: "次に食べたいハンバーガーが見つかる"
      expect(page).to have_selector '#q_name_cont'
      expect(page).to have_selector '#q_address_or_station_cont'
      expect(page).to have_selector 'input[value="検索"]'
      expect(page).to have_selector 'footer p', text: '© 2019 Shogo'

      within '.popular-wrapper' do
        expect(page).to have_selector 'h3', text: "人気"
        expect(page).to have_selector '.d-flex a', text: "もっと見る"
        expect(page).to have_selector "#shop-#{shops[0].id} .card"
        expect(page).to have_selector "#shop-#{shops[1].id} .card"
        expect(page).to have_selector "#shop-#{shops[2].id} .card"
        expect(page).to have_selector "#shop-#{shops[3].id} .card"
      end

      within '.new-wrapper' do
        expect(page).to have_selector 'h3', text: "新着"
        expect(page).to have_selector '.d-flex a', text: "もっと見る"
        expect(page).to have_selector "#shop-#{shops[0].id} .card"
        expect(page).to have_selector "#shop-#{shops[1].id} .card"
        expect(page).to have_selector "#shop-#{shops[2].id} .card"
        expect(page).to have_selector "#shop-#{shops[3].id} .card"
      end

      within '.topuser-wrapper' do
        expect(page).to have_selector 'h3', text: "トップユーザー"
        expect(page).to have_selector '.d-flex a', text: "もっと見る"
        expect(page).to have_selector "#user-#{users[0].id} .card"
        expect(page).to have_selector "#user-#{users[1].id} .card"
        expect(page).to have_selector "#user-#{users[2].id} .card"
        expect(page).to have_selector "#user-#{users[3].id} .card"
      end
    end

    scenario '人気のお店をクリックして、詳細ページに遷移する' do
      within '.popular-wrapper' do
        find("#shop-#{shops[0].id}").click
      end

      expect(page).to have_current_path shop_path(shops[0])
    end

    scenario '新着のお店をクリックして、詳細ページに遷移する' do
      within '.new-wrapper' do
        find("#shop-#{shops[1].id}").click
      end

      expect(page).to have_current_path shop_path(shops[1])
    end

    scenario 'トップユーザーをクリックして、詳細ページに遷移する' do
      within '.topuser-wrapper' do
        find("#user-#{users[3].id}").click
      end

      expect(page).to have_current_path user_path(users[3])
    end
  end

  feature 'トップページからお店検索' do
    given!(:shop_a) { create(:shop, name: "テストバーガー原宿", station: "原宿駅", address: "東京都渋谷区神宮前1-1-1") }
    given!(:shop_b) { create(:shop, name: "テストバーガー池袋", station: "池袋駅", address: " 東京都豊島区南池袋1-1-1") }

    background do
      visit root_path
    end

    scenario 'お店を名前で検索する' do
      fill_in 'q_name_cont', with: 'テストバーガー'
      click_button '検索'

      expect(page).to have_content '2件のお店が表示されています'
      expect(page).to have_selector "#shop-#{shop_a.id} h5", text: 'テストバーガー原宿'
      expect(page).to have_selector "#shop-#{shop_b.id} h5", text: 'テストバーガー池袋'
    end

    scenario 'お店を駅名で検索する' do
      fill_in 'q_address_or_station_cont', with: '池袋駅'
      click_button '検索'

      expect(page).to have_content '1件のお店が表示されています'
      expect(page).to have_selector "#shop-#{shop_b.id} h5", text: 'テストバーガー池袋'
    end

    scenario 'お店を住所で検索する' do
      fill_in 'q_address_or_station_cont', with: '神宮前'
      click_button '検索'

      expect(page).to have_content '1件のお店が表示されています'
      expect(page).to have_selector "#shop-#{shop_a.id} h5", text: 'テストバーガー原宿'
    end

    scenario 'お店を名前・駅名で複合検索する' do
      fill_in 'q_name_cont', with: 'テストバーガー'
      fill_in 'q_address_or_station_cont', with: '原宿'
      click_button '検索'

      expect(page).to have_content '1件のお店が表示されています'
      expect(page).to have_selector "#shop-#{shop_a.id} h5", text: 'テストバーガー原宿'
    end
  end
end
