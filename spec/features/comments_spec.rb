require 'rails_helper'

RSpec.feature "Comments", type: :feature do
  feature "コメント作成" do
    include ActionView::Helpers::DateHelper
    include ApplicationHelper
    include PostsHelper

    given(:user) { create(:user) }
    given(:post) { create(:post, user: user) }

    background do
      sign_in user
      visit post_path(post)
    end

    scenario "コメントを作成する" do
      expect(page).to have_selector ".comment-t", text: "0"
      within '.comment-wrapper' do
        expect(page).to have_selector "h5", text: "コメント 0件"
        expect(page).to have_selector "h5", text: "コメント投稿"
      end

      expect do
        fill_in 'comment_content', with: 'テスト'
        click_on '送信'

        comment = user.comments.last

        expect(page).to have_selector '.alert-success', text: "コメントしました。"
        expect(page).to have_selector ".comment-t", text: "1"
        within '.comment-wrapper' do
          expect(page).to have_selector "h5", text: "コメント 1件"
          expect(page).to have_selector ".col-1 img"
          expect(page).to have_link '太郎'
          expect(page).to have_selector ".col-10 p", text: "テスト"
          expect(page).to have_selector "p.small", text: post_time(comment.created_at)
          expect(page).to have_link '削除する'
        end
      end.to change { user.comments.count }.by(1)
    end
  end

  feature "コメント削除" do
    given(:user) { create(:user) }
    given(:post) { create(:post, user: user) }
    given!(:comment) { create(:comment, user: user, post: post) }

    background do
      sign_in user
      visit post_path(post)
    end

    scenario "コメントを削除する", js: true do
      expect do
        click_on '削除する'
        page.driver.browser.switch_to.alert.accept

        expect(page).to have_current_path post_path(post)
        expect(page).to have_selector '.alert-success', text: "コメントを削除しました。"
      end.to change { user.comments.count }.by(-1)
    end
  end
end
