require 'rails_helper'

RSpec.feature "Shops", type: :feature do
  feature "お店一覧ページ" do
    given!(:shops) { create_list(:shop, 3) }
    given!(:one_shop) { create(:shop, :with_bookmark_and_post) }

    background do
      visit shops_path
    end

    scenario "一覧ページを表示する" do
      expect(page).to have_current_path shops_path
      expect(page).to have_title 'お店を探す | Burger Mania!'
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_selector '.current', text: 'お店を探す'
      end
      expect(page).to have_selector 'h2', text: 'お店を探す'
      expect(page).to have_selector '#q_name_cont'
      expect(page).to have_selector '#q_address_or_station_cont'
      expect(page).to have_selector 'input[value="検索"]'
      expect(page).to have_link 'お店を登録する'
      expect(page).to have_content '4件のお店が表示されています'
      expect(all('.card').size).to eq(4)
      within "#shop-#{shops[0].id}" do
        expect(page).to have_selector "h5", text: 'ハンバーガー屋'
        expect(page).to have_selector ".fa-subway"
        expect(page).to have_selector "p", text: '渋谷駅'
        expect(page).to have_selector ".fa-map-marker-alt"
        expect(page).to have_selector "p", text: '東京都渋谷区宇田川町1-1'
        expect(page).to have_selector '.fa-star'
        expect(page).to have_selector '.card-footer .bookmark-t', text: '0'
        expect(page).to have_selector '.fa-comment-dots'
        expect(page).to have_selector '.card-footer .post-t', text: '0'
      end

      within "#shop-#{one_shop.id}" do
        expect(page).to have_selector "h5", text: 'ハンバーガー屋'
        expect(page).to have_selector ".fa-subway"
        expect(page).to have_selector "p", text: '渋谷駅'
        expect(page).to have_selector ".fa-map-marker-alt"
        expect(page).to have_selector "p", text: '東京都渋谷区宇田川町1-1'
        expect(page).to have_selector '.fa-star'
        expect(page).to have_selector '.card-footer .bookmark-t', text: '1'
        expect(page).to have_selector '.fa-comment-dots'
        expect(page).to have_selector '.card-footer .post-t', text: '1'
      end
    end

    scenario "お店をクリックして詳細ページに遷移する" do
      find("#shop-#{shops[0].id}").click

      expect(page).to have_current_path shop_path(shops[0])
    end

    scenario "お店を登録するをクリックして登録ページに遷移する" do
      user = create(:user)
      sign_in user

      click_link 'お店を登録する'

      expect(page).to have_current_path new_shop_path
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end
  end

  feature 'お店一覧で検索' do
    given!(:shop_a) { create(:shop, name: "テストバーガー原宿", station: "原宿駅", address: "東京都渋谷区神宮前1-1-1") }
    given!(:shop_b) { create(:shop, name: "テストバーガー池袋", station: "池袋駅", address: " 東京都豊島区南池袋1-1-1") }

    background do
      visit root_path
    end

    scenario 'お店を名前で検索する' do
      fill_in 'q_name_cont', with: 'テストバーガー'
      click_button '検索'

      expect(page).to have_content '2件のお店が表示されています'
      expect(page).to have_selector "#shop-#{shop_a.id} h5", text: 'テストバーガー原宿'
      expect(page).to have_selector "#shop-#{shop_b.id} h5", text: 'テストバーガー池袋'
    end

    scenario 'お店を駅名で検索する' do
      fill_in 'q_address_or_station_cont', with: '池袋駅'
      click_button '検索'

      expect(page).to have_content '1件のお店が表示されています'
      expect(page).to have_selector "#shop-#{shop_b.id} h5", text: 'テストバーガー池袋'
    end

    scenario 'お店を住所で検索する' do
      fill_in 'q_address_or_station_cont', with: '神宮前'
      click_button '検索'

      expect(page).to have_content '1件のお店が表示されています'
      expect(page).to have_selector "#shop-#{shop_a.id} h5", text: 'テストバーガー原宿'
    end

    scenario 'お店を名前・駅名で複合検索する' do
      fill_in 'q_name_cont', with: 'テストバーガー'
      fill_in 'q_address_or_station_cont', with: '原宿'
      click_button '検索'

      expect(page).to have_content '1件のお店が表示されています'
      expect(page).to have_selector "#shop-#{shop_a.id} h5", text: 'テストバーガー原宿'
    end
  end

  feature 'お店一覧のページネーション' do
    given!(:shops) { create_list(:shop, 21) }

    background do
      visit shops_path
    end

    scenario "ページネーションで遷移する" do
      expect(page).to have_selector 'ul.pagination'
      expect(all('ul.pagination').size).to eq(2)
      expect(page).to have_selector 'li.active', text: '1'
      expect(page).to have_selector 'li.page-item', text: '2'
      expect(page).to have_selector 'li.page-item', text: '次 ›'
      expect(page).to have_selector 'li.page-item', text: '最後 »'
      expect(page).to have_content '全21件中 1 - 20件のお店が表示されています'
      expect(all('.card').size).to eq(20)
      click_link '2', match: :first

      expect(page).to have_current_path '/shops?page=2'
      expect(page).to have_selector 'li.active', text: '2'
      expect(page).to have_content '全21件中 21 - 21件のお店が表示されています'
      expect(all('.card').size).to eq(1)
      click_link '1', match: :first

      expect(page).to have_current_path '/shops'
      click_link '次', match: :first

      expect(page).to have_current_path '/shops?page=2'
      click_link '前', match: :first

      expect(page).to have_current_path '/shops'
      click_link '最後', match: :first

      expect(page).to have_current_path '/shops?page=2'
      click_link '最初', match: :first

      expect(page).to have_current_path '/shops'
    end
  end

  feature 'お店詳細ページ' do
    include ActionView::Helpers::DateHelper
    include ApplicationHelper
    include PostsHelper

    given!(:user) { create(:user) }
    given!(:shop) { create(:shop, :with_post) }

    background do
      @post = shop.posts.first

      sign_in user
      visit shop_path(shop)
    end

    scenario "詳細ページを表示する" do
      expect(page).to have_current_path shop_path(shop)
      expect(page).to have_title 'ハンバーガー屋 | Burger Mania!'
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'お店を探す'
        expect(page).to have_selector '.current', text: 'ハンバーガー屋'
      end
      expect(page).to have_selector 'img.card-img-top'
      expect(page).to have_selector 'h2', text: 'ハンバーガー屋'
      expect(page).to have_selector '.bookmark .fa-star'
      expect(page).to have_selector '.bookmark', text: '0'

      within '.post-wrapper' do
        expect(page).to have_selector 'h3', text: 'レビュー'
        expect(all('.post-card-t').size).to eq(1)
        within "#post-#{@post.id}" do
          expect(page).to have_selector "img"
          expect(page).to have_selector "p b", text: '太郎'
          expect(page).to have_selector "p", text: "お店：ハンバーガー屋"
          expect(page).to have_selector "div h3", text: "テスト"
          expect(page).to have_selector "div p", text: "テスト"
          expect(page).to have_selector "div p", text: "おすすめメニュー：ハンバーガー"
          expect(page).to have_selector ".comment-t .fa-comments"
          expect(page).to have_selector ".comment-t", text: "0"
          expect(page).to have_selector ".like-t .fa-heart"
          expect(page).to have_selector ".like-t", text: "0"
          expect(page).to have_selector "p.col-7", text: post_time(@post.updated_at)
        end
        expect(page).to have_link 'レビューを投稿する'
      end

      within '.info-wrapper' do
        expect(page).to have_selector 'h3', text: '基本情報'
        expect(page).to have_selector 'th', text: '店名'
        expect(page).to have_selector 'td', text: 'ハンバーガー屋'

        expect(page).to have_selector 'th', text: 'メニュー'
        expect(page).to have_selector 'td', text: 'ハンバーガー、チーズバーガー、ポテトフライ'

        expect(page).to have_selector 'th', text: '営業時間'
        expect(page).to have_selector 'td', text: '11:00-22:00'

        expect(page).to have_selector 'th', text: '定休日'
        expect(page).to have_selector 'td', text: '水曜日'

        expect(page).to have_selector 'th', text: '最寄駅'
        expect(page).to have_selector 'td', text: '渋谷駅'

        expect(page).to have_selector 'th', text: '住所'
        expect(page).to have_selector 'td', text: '東京都渋谷区宇田川町1-1'

        expect(page).to have_selector 'th', text: '電話番号'
        expect(page).to have_selector 'td', text: '080-1111-1111'

        expect(page).to have_selector 'th', text: 'URL'
        expect(page).to have_selector 'td', text: 'https://www.example.com'

        expect(page).to have_selector 'th', text: '更新日時'
        expect(page).to have_selector 'td', text: simple_time(shop.updated_at)
        expect(page).to have_link 'お店の情報を編集する'
      end
    end

    scenario "レビュー詳細ページに遷移する" do
      find("#post-#{@post.id}").click

      expect(page).to have_current_path post_path(@post)
    end

    scenario "レビュー一覧ページに遷移する" do
      create_list(:post, 4, shop: shop)
      visit current_path

      within ".post-wrapper" do
        click_link "もっと見る"
      end

      expect(page).to have_current_path shop_posts_path(shop)
      expect(page).to have_selector 'h2', text: 'ハンバーガー屋のレビュー一覧'
      expect(page).to have_content '5件のレビューが表示されています'
      expect(all('.post-card-t').size).to eq(5)
    end

    scenario "レビュー投稿ページに遷移する" do
      click_link 'レビューを投稿する'

      expect(page).to have_current_path new_shop_post_path(shop)
    end

    scenario "お店編集ページに遷移する" do
      click_link 'お店の情報を編集する'

      expect(page).to have_current_path edit_shop_path(shop)
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、お店一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'お店を探す'
      end

      expect(page).to have_current_path shops_path
    end
  end

  feature 'お店登録ページ' do
    given!(:user) { create(:user) }
    given!(:shop) { create(:shop) }

    background do
      sign_in user
      visit new_shop_path(shop)
    end

    scenario "登録ページを表示する" do
      expect(page).to have_current_path new_shop_path(shop)
      expect(page).to have_title 'お店登録 | Burger Mania!'
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_selector '.current', text: 'お店登録'
      end
      expect(page).to have_selector 'h2', text: 'お店登録'
      expect(page).to have_selector 'label', text: '店名'
      expect(page).to have_selector 'input#shop_name'
      expect(page).to have_selector 'label', text: 'メニュー'
      expect(page).to have_selector 'textarea#shop_menu'
      expect(page).to have_selector 'label', text: '営業時間'
      expect(page).to have_selector 'input#shop_hour'
      expect(page).to have_selector 'label', text: '定休日'
      expect(page).to have_selector 'input#shop_holiday'
      expect(page).to have_selector 'label', text: '最寄駅'
      expect(page).to have_selector 'input#shop_station'
      expect(page).to have_selector 'label', text: '住所'
      expect(page).to have_selector 'input#shop_address'
      expect(page).to have_selector 'label', text: '電話番号'
      expect(page).to have_selector 'input#shop_phone'
      expect(page).to have_selector 'label', text: 'URL'
      expect(page).to have_selector 'input#shop_url'
      expect(page).to have_selector 'label', text: '写真'
      expect(page).to have_selector 'input#shop_picture'
      expect(page).to have_button '登録する'
    end

    scenario "お店を登録する" do
      expect do
        fill_in '店名', with: 'テストバーガー'
        fill_in 'メニュー', with: 'ハンバーガー'
        fill_in '営業時間', with: '12:00-20:00'
        fill_in '定休日', with: '日曜日'
        fill_in '最寄駅', with: '原宿駅'
        fill_in '住所', with: '東京都渋谷区神宮前1-1-1'
        fill_in '電話番号', with: '090-1111-1111'
        fill_in 'URL', with: 'https://www.example.com'
        attach_file '写真', "#{Rails.root}/spec/factories/test.jpg"
        click_button '登録する'

        new_shop = Shop.last

        expect(page).to have_current_path shop_path(new_shop)
        expect(page).to have_selector '.alert-success', text: 'お店「テストバーガー」を登録しました。'
      end.to change(Shop, :count).by(1)
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end
  end

  feature 'お店編集ページ' do
    given!(:user) { create(:user) }
    given!(:shop) { create(:shop) }

    background do
      sign_in user
      visit edit_shop_path(shop)
    end

    scenario "編集ページを表示する" do
      expect(page).to have_current_path edit_shop_path(shop)
      expect(page).to have_title 'お店編集 | Burger Mania!'

      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'お店を探す'
        expect(page).to have_link 'ハンバーガー屋'
        expect(page).to have_selector '.current', text: 'お店編集'
      end

      expect(page).to have_selector 'h2', text: 'お店編集'
      expect(page).to have_field '店名', with: 'ハンバーガー屋'
      expect(page).to have_field 'メニュー', with: 'ハンバーガー、チーズバーガー、ポテトフライ'
      expect(page).to have_field '営業時間', with: '11:00-22:00'
      expect(page).to have_field '定休日', with: '水曜日'
      expect(page).to have_field '最寄駅', with: '渋谷駅'
      expect(page).to have_field '住所', with: '東京都渋谷区宇田川町1-1'
      expect(page).to have_field '電話番号', with: '080-1111-1111'
      expect(page).to have_field 'URL', with: 'https://www.example.com'
      expect(page).to have_selector 'label', text: '写真'
      expect(page).to have_css("img[src*='test.jpg']")
      expect(page).to have_selector 'input#shop_picture'
      expect(page).to have_selector 'input#shop_picture_cache', visible: false
      expect(page).to have_button '更新する'
    end

    scenario "お店の情報を更新する" do
      fill_in '店名', with: 'テストバーガー'
      fill_in 'メニュー', with: 'スペシャルバーガー'
      fill_in '営業時間', with: '12:00-20:00'
      fill_in '定休日', with: '日曜日'
      fill_in '最寄駅', with: '原宿駅'
      fill_in '住所', with: '東京都渋谷区神宮前1-1-1'
      fill_in '電話番号', with: '090-1111-1111'
      fill_in 'URL', with: 'https://www.test.com'
      attach_file '写真', "#{Rails.root}/spec/factories/test_avatar.jpg"
      click_button '更新する'

      expect(page).to have_current_path shop_path(shop)
      expect(page).to have_selector '.alert-success', text: 'お店「テストバーガー」を更新しました。'
      click_on "お店の情報を編集する"

      expect(page).to have_field '店名', with: 'テストバーガー'
      expect(page).to have_field 'メニュー', with: 'スペシャルバーガー'
      expect(page).to have_field '営業時間', with: '12:00-20:00'
      expect(page).to have_field '定休日', with: '日曜日'
      expect(page).to have_field '最寄駅', with: '原宿駅'
      expect(page).to have_field '住所', with: '東京都渋谷区神宮前1-1-1'
      expect(page).to have_field '電話番号', with: '090-1111-1111'
      expect(page).to have_field 'URL', with: 'https://www.test.com'
      expect(page).to have_css("img[src*='test_avatar.jpg']")
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、お店一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'お店を探す'
      end

      expect(page).to have_current_path shops_path
    end

    scenario 'パンくずリストをクリックして、お店詳細ページに戻る' do
      within '.breadcrumbs' do
        click_link shop.name
      end

      expect(page).to have_current_path shop_path(shop)
    end
  end

  feature 'お店の削除' do
    context '管理者権限を持っている場合' do
      given!(:admin_user) { create(:admin_user) }
      given!(:shop) { create(:shop) }

      background do
        sign_in admin_user
        visit shop_path(shop)
      end

      scenario 'お店を削除する', js: true do
        expect do
          click_on '削除'
          page.driver.browser.switch_to.alert.accept

          expect(page).to have_current_path shops_path
          expect(page).to have_selector '.alert-success', text: "お店「ハンバーガー屋」を削除しました。"
        end.to change(Shop, :count).by(-1)
      end
    end

    context '管理者権限を持っていない場合' do
      given!(:user) { create(:user) }
      given!(:shop) { create(:shop) }

      background do
        sign_in user
        visit shop_path(shop)
      end

      scenario 'お店を削除するリンクが表示されない' do
        expect(page).not_to have_link '削除'
      end
    end
  end
end
