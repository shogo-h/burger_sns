require 'rails_helper'

RSpec.feature "Likes", type: :feature do
  feature 'いいね作成' do
    given(:user) { create(:user) }
    given(:post) { create(:post) }

    background do
      sign_in user
      visit post_path(post)
    end

    scenario 'レビューにいいねする' do
      expect(page).to have_selector ".like-t", text: "0"
      expect(page).to have_selector "i.far.fa-heart"

      expect do
        find('.like-t a').click

        expect(page).to have_current_path post_path(post)
        expect(page).to have_selector ".like-t", text: "1"
        expect(page).to have_selector "i.fas.fa-heart"
      end.to change { user.likes.count }.by(1)
    end
  end

  feature 'いいね削除' do
    given(:user) { create(:user) }
    given(:post) { create(:post) }
    given!(:like) { create(:like, user: user, post: post) }

    background do
      sign_in user
      visit post_path(post)
    end

    scenario 'レビューにしたいいねを削除する' do
      expect(page).to have_selector ".like-t", text: "1"
      expect(page).to have_selector "i.fas.fa-heart"

      expect do
        find('.like-t a').click

        expect(page).to have_current_path post_path(post)
        expect(page).to have_selector ".like-t", text: "0"
        expect(page).to have_selector "i.far.fa-heart"
      end.to change { user.likes.count }.by(-1)
    end
  end
end
