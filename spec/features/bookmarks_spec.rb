require 'rails_helper'

RSpec.feature "Bookmarks", type: :feature do
  feature '行きたいお店保存' do
    given(:user) { create(:user) }
    given(:shop) { create(:shop) }

    background do
      sign_in user
      visit shop_path(shop)
    end

    scenario '行きたいお店として保存する' do
      expect(page).to have_selector ".bookmark", text: "0"
      expect(page).to have_selector "i.far.fa-star"

      expect do
        find('.bookmark a').click

        expect(page).to have_current_path shop_path(shop)
        expect(page).to have_selector ".bookmark", text: "1"
        expect(page).to have_selector "i.fas.fa-star"
      end.to change { user.bookmarks.count }.by(1)
    end
  end

  feature '行きたいお店削除' do
    given(:user) { create(:user) }
    given(:shop) { create(:shop) }
    given!(:bookmark) { create(:bookmark, user: user, shop: shop) }

    background do
      sign_in user
      visit shop_path(shop)
    end

    scenario '行きたいお店から削除する' do
      expect(page).to have_selector ".bookmark", text: "1"
      expect(page).to have_selector "i.fas.fa-star"

      expect do
        find('.bookmark a').click

        expect(page).to have_current_path shop_path(shop)
        expect(page).to have_selector ".bookmark", text: "0"
        expect(page).to have_selector "i.far.fa-star"
      end.to change { user.bookmarks.count }.by(-1)
    end
  end
end
