require 'rails_helper'

RSpec.feature "Posts", type: :feature do
  feature 'レビュー一覧ページ' do
    include ActionView::Helpers::DateHelper
    include ApplicationHelper
    include PostsHelper

    given!(:shop) { create(:shop) }
    given!(:posts) { create_list(:post, 4, shop: shop) }

    background do
      visit shop_posts_path(shop)
    end

    scenario 'レビュー一覧を表示する' do
      expect(page).to have_current_path shop_posts_path(shop)
      expect(page).to have_title 'ハンバーガー屋のレビュー一覧 | Burger Mania!'
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'お店を探す'
        expect(page).to have_link 'ハンバーガー屋'
        expect(page).to have_selector '.current', text: 'レビュー一覧'
      end
      expect(page).to have_selector 'h2', text: 'ハンバーガー屋のレビュー一覧'
      expect(page).to have_link 'お店に戻る'
      expect(page).to have_content '4件のレビューが表示されています'
      expect(all('.post-card-t').size).to eq(4)
      within "#post-#{posts[0].id}" do
        expect(page).to have_selector ".col-md-1 img"
        expect(page).to have_selector "p b", text: '太郎'
        expect(page).to have_selector "p", text: "お店：ハンバーガー屋"
        expect(page).to have_selector "div h3", text: "テスト"
        expect(page).to have_selector "div p", text: "テスト"
        expect(page).to have_selector "div p", text: "おすすめメニュー：ハンバーガー"
        expect(page).to have_selector ".col-md-11 img"
        expect(page).to have_selector ".comment-t .fa-comments"
        expect(page).to have_selector ".comment-t", text: "0"
        expect(page).to have_selector ".like-t .fa-heart"
        expect(page).to have_selector ".like-t", text: "0"
        expect(page).to have_selector "p.col-7", text: post_time(posts[0].updated_at)
      end
      expect(page).to have_link 'レビューを投稿する'
    end

    scenario 'レビューを選択して、詳細ページに遷移する' do
      find("#post-#{posts[0].id}").click

      expect(page).to have_current_path post_path(posts[0])
    end

    scenario "レビューを投稿するをクリックして投稿ページに遷移する" do
      user = create(:user)
      sign_in user

      click_link "レビューを投稿する"

      expect(page).to have_current_path new_shop_post_path(shop)
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、お店一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'お店を探す'
      end

      expect(page).to have_current_path shops_path
    end

    scenario 'パンくずリストをクリックして、お店詳細ページに戻る' do
      within '.breadcrumbs' do
        click_link 'ハンバーガー屋'
      end

      expect(page).to have_current_path shop_path(shop)
    end
  end

  feature 'レビュー詳細' do
    include ActionView::Helpers::DateHelper
    include ApplicationHelper
    include PostsHelper

    given!(:user) { create(:user) }
    given!(:shop) { create(:shop) }
    given!(:post) { create(:post, user: user, shop: shop) }

    background do
      sign_in user
      visit post_path(post)
    end

    scenario '詳細ページを表示する' do
      expect(page).to have_current_path post_path(post)
      expect(page).to have_title 'ハンバーガー屋のレビュー詳細 | Burger Mania!'
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'お店を探す'
        expect(page).to have_link 'ハンバーガー屋'
        expect(page).to have_link 'レビュー一覧'
        expect(page).to have_selector '.current', text: 'レビュー詳細'
      end
      expect(page).to have_selector 'h2', text: 'ハンバーガー屋のレビュー詳細'
      expect(page).to have_selector ".col-md-1 img"
      expect(page).to have_selector "p b", text: '太郎'
      expect(page).to have_selector "p", text: "お店：ハンバーガー屋"
      expect(page).to have_selector "div h3", text: "テスト"
      expect(page).to have_selector "div p", text: "テスト"
      expect(page).to have_selector "div p", text: "おすすめメニュー：ハンバーガー"
      expect(page).to have_selector ".col-md-11 img"
      expect(page).to have_selector ".comment-t .fa-comments"
      expect(page).to have_selector ".comment-t", text: "0"
      expect(page).to have_selector ".like-t .fa-heart"
      expect(page).to have_selector ".like-t", text: "0"
      expect(page).to have_selector "p.col-7", text: post_time(post.updated_at)
    end

    scenario 'ユーザーをクリックして、ユーザー詳細ページに遷移する' do
      click_link '太郎'

      expect(page).to have_current_path user_path(user)
    end

    scenario '編集をクリックして、編集ページに遷移する' do
      click_on '編集'

      expect(page).to have_current_path edit_post_path(post)
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、お店一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'お店を探す'
      end

      expect(page).to have_current_path shops_path
    end

    scenario 'パンくずリストをクリックして、お店詳細ページに戻る' do
      within '.breadcrumbs' do
        click_link 'ハンバーガー屋'
      end

      expect(page).to have_current_path shop_path(shop)
    end

    scenario 'パンくずリストをクリックして、レビュー一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'レビュー一覧'
      end

      expect(page).to have_current_path shop_posts_path(shop)
    end
  end

  feature 'レビュー投稿ページ' do
    given!(:user) { create(:user) }
    given!(:shop) { create(:shop) }

    background do
      sign_in user
      visit new_shop_post_path(shop)
    end

    scenario '投稿ページを表示する' do
      expect(page).to have_current_path new_shop_post_path(shop)
      expect(page).to have_title 'ハンバーガー屋のレビュー投稿 | Burger Mania!'
      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'お店を探す'
        expect(page).to have_link 'ハンバーガー屋'
        expect(page).to have_selector '.current', text: 'レビュー投稿'
      end
      expect(page).to have_selector 'h2', text: 'ハンバーガー屋のレビュー投稿'
      expect(page).to have_selector 'label', text: 'タイトル'
      expect(page).to have_selector 'input#post_title'
      expect(page).to have_selector 'label', text: '内容'
      expect(page).to have_selector 'textarea#post_description'
      expect(page).to have_selector 'label', text: 'おすすめメニュー'
      expect(page).to have_selector 'input#post_recomend'
      expect(page).to have_selector 'label', text: '写真'
      expect(page).to have_selector 'input#post_picture'
      expect(page).to have_button '投稿する'
    end

    scenario 'レビューを投稿する' do
      expect do
        fill_in 'タイトル', with: 'テスト'
        fill_in '内容', with: 'テストです'
        fill_in 'おすすめメニュー', with: 'ハンバーガー'
        attach_file '写真', "#{Rails.root}/spec/factories/test.jpg"
        click_button '投稿する'

        new_post = user.posts.last

        expect(page).to have_current_path post_path(new_post)
        expect(page).to have_selector '.alert-success', text: 'レビュー「テスト」を投稿しました。'
      end.to change(Post, :count).by(1)
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、お店一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'お店を探す'
      end

      expect(page).to have_current_path shops_path
    end

    scenario 'パンくずリストをクリックして、お店詳細ページに戻る' do
      within '.breadcrumbs' do
        click_link 'ハンバーガー屋'
      end

      expect(page).to have_current_path shop_path(shop)
    end
  end

  feature 'レビュー編集ページ' do
    given!(:user) { create(:user) }
    given!(:shop) { create(:shop) }
    given!(:post) { create(:post, user: user, shop: shop) }

    background do
      sign_in user
      visit edit_post_path(post)
    end

    scenario '編集ページを表示する' do
      expect(page).to have_current_path edit_post_path(post)
      expect(page).to have_title 'ハンバーガー屋のレビュー編集 | Burger Mania!'

      within '.breadcrumbs' do
        expect(page).to have_link 'Home'
        expect(page).to have_link 'お店を探す'
        expect(page).to have_link 'ハンバーガー屋'
        expect(page).to have_selector '.current', text: 'レビュー編集'
      end

      expect(page).to have_selector 'h2', text: 'ハンバーガー屋のレビュー編集'
      expect(page).to have_field 'タイトル', with: 'テスト'
      expect(page).to have_field '内容', with: 'テスト'
      expect(page).to have_field 'おすすめメニュー', with: 'ハンバーガー'
      expect(page).to have_selector 'label', text: '写真'
      expect(page).to have_css("img[src*='test.jpg']")
      expect(page).to have_selector 'input#post_picture'
      expect(page).to have_selector 'input#post_picture_cache', visible: false
      expect(page).to have_button '更新する'
    end

    scenario 'レビューを編集する' do
      fill_in 'タイトル', with: 'おいしい'
      fill_in '内容', with: 'おいしいです'
      fill_in 'おすすめメニュー', with: 'チーズバーガー'
      attach_file '写真', "#{Rails.root}/spec/factories/test_avatar.jpg"
      click_button '更新する'

      expect(page).to have_current_path post_path(post)
      expect(page).to have_selector '.alert-success', text: 'レビュー「おいしい」を更新しました。'
      click_on "編集"

      expect(page).to have_field 'タイトル', with: 'おいしい'
      expect(page).to have_field '内容', with: 'おいしいです'
      expect(page).to have_field 'おすすめメニュー', with: 'チーズバーガー'
      expect(page).to have_css("img[src*='test_avatar.jpg']")
    end

    scenario 'パンくずリストをクリックして、トップページに戻る' do
      within '.breadcrumbs' do
        click_link 'Home'
      end

      expect(page).to have_current_path root_path
    end

    scenario 'パンくずリストをクリックして、お店一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'お店を探す'
      end

      expect(page).to have_current_path shops_path
    end

    scenario 'パンくずリストをクリックして、お店詳細ページに戻る' do
      within '.breadcrumbs' do
        click_link 'ハンバーガー屋'
      end

      expect(page).to have_current_path shop_path(shop)
    end

    scenario 'パンくずリストをクリックして、レビュー一覧ページに戻る' do
      within '.breadcrumbs' do
        click_link 'レビュー一覧'
      end

      expect(page).to have_current_path shop_posts_path(shop)
    end

    scenario 'パンくずリストをクリックして、レビュー詳細ページに戻る' do
      within '.breadcrumbs' do
        click_link 'レビュー詳細'
      end

      expect(page).to have_current_path post_path(post)
    end
  end

  feature 'レビュー削除' do
    given!(:user) { create(:user) }
    given!(:post) { create(:post, user: user) }

    background do
      sign_in user
      visit post_path(post)
    end

    scenario '削除する', js: true do
      expect do
        click_on '削除'
        page.driver.browser.switch_to.alert.accept

        expect(page).to have_current_path root_path
        expect(page).to have_selector '.alert-success', text: "レビュー「テスト」を削除しました。"
      end.to change(Post, :count).by(-1)
    end
  end

  feature 'タイムライン' do
    context 'フォローしているユーザーがいる場合' do
      include ActionView::Helpers::DateHelper
      include ApplicationHelper
      include PostsHelper

      given!(:users) { create_list(:user, 3) }
      given!(:posts_a) { create_list(:post, 2, user: users[1]) }
      given!(:posts_b) { create_list(:post, 2, user: users[2]) }

      background do
        Relationship.create(follower: users[0], followed: users[1])
        Relationship.create(follower: users[0], followed: users[2])

        sign_in users[0]
        visit timeline_path
      end

      scenario 'タイムラインを表示する' do
        expect(page).to have_current_path timeline_path
        expect(page).to have_title 'タイムライン | Burger Mania!'
        within '.breadcrumbs' do
          expect(page).to have_link 'Home'
          expect(page).to have_selector '.current', text: 'タイムライン'
        end
        expect(page).to have_selector 'h2', text: 'タイムライン'
        expect(page).to have_content '4件のレビューが表示されています'
        expect(all('.post-card-t').size).to eq(4)
        within "#post-#{posts_a[0].id}" do
          expect(page).to have_selector ".col-md-1 img"
          expect(page).to have_selector "p b", text: '太郎'
          expect(page).to have_selector "p", text: "お店：ハンバーガー屋"
          expect(page).to have_selector "div h3", text: "テスト"
          expect(page).to have_selector "div p", text: "テスト"
          expect(page).to have_selector "div p", text: "おすすめメニュー：ハンバーガー"
          expect(page).to have_selector ".col-md-11 img"
          expect(page).to have_selector ".comment-t .fa-comments"
          expect(page).to have_selector ".comment-t", text: "0"
          expect(page).to have_selector ".like-t .fa-heart"
          expect(page).to have_selector ".like-t", text: "0"
          expect(page).to have_selector "p.col-7", text: post_time(posts_a[0].updated_at)
        end
      end

      scenario 'レビューを選択して、詳細ページに遷移する' do
        find("#post-#{posts_a[0].id}").click

        expect(page).to have_current_path post_path(posts_a[0])
      end

      scenario 'パンくずリストをクリックして、トップページに戻る' do
        within '.breadcrumbs' do
          click_link 'Home'
        end

        expect(page).to have_current_path root_path
      end
    end

    context 'フォローしているユーザーがいない場合' do
      given(:user) { create(:user) }
      given!(:other_user) { create(:user, username: "花子", introduction: "ポテトが好き") }

      background do
        sign_in user
        visit timeline_path
      end

      scenario 'タイムラインが表示されない' do
        expect(page).to have_current_path timeline_path
        expect(page).to have_title 'タイムライン | Burger Mania!'
        within '.breadcrumbs' do
          expect(page).to have_link 'Home'
          expect(page).to have_selector '.current', text: 'タイムライン'
        end
        expect(page).to have_selector 'h2', text: 'タイムライン'
        expect(page).to have_content 'レビューがありません'
        expect(page).to have_selector 'p', text: 'ユーザーをフォローしましょう'
        expect(page).to have_selector 'input#q_username_or_introduction_cont'
        expect(page).to have_selector 'input[value="検索"]'
      end

      scenario 'ユーザーを名前で検索する' do
        fill_in 'q_username_or_introduction_cont', with: '花子'
        click_button '検索'

        expect(page).to have_content '1件のユーザーが表示されています'
        expect(page).to have_selector "#user-#{other_user.id} h5", text: '花子'
      end

      scenario 'ユーザーを自己紹介の内容で検索する' do
        fill_in 'q_username_or_introduction_cont', with: 'ポテト'
        click_button '検索'

        expect(page).to have_content '1件のユーザーが表示されています'
        expect(page).to have_selector "#user-#{other_user.id} .introduction", text: 'ポテトが好き'
      end

      scenario 'パンくずリストをクリックして、トップページに戻る' do
        within '.breadcrumbs' do
          click_link 'Home'
        end

        expect(page).to have_current_path root_path
      end
    end
  end
end
