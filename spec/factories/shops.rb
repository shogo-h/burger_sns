FactoryBot.define do
  factory :shop do
    name { "ハンバーガー屋" }
    menu { "ハンバーガー、チーズバーガー、ポテトフライ" }
    hour { "11:00-22:00" }
    holiday { "水曜日" }
    address { "東京都渋谷区宇田川町1-1" }
    phone { "080-1111-1111" }
    url { "https://www.example.com" }
    picture do
      Rack::Test::UploadedFile.new(Rails.root.join('spec/factories/test.jpg'), 'image/jpeg')
    end
    station { "渋谷駅" }

    trait :with_bookmark do
      after(:create) { |shop| create(:bookmark, shop: shop) }
    end

    trait :with_post do
      after(:create) { |shop| create(:post, shop: shop) }
    end

    trait :with_bookmark_and_post do
      after(:create) do |shop|
        create(:bookmark, shop: shop)
        create(:post, shop: shop)
      end
    end
  end
end
