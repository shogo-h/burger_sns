FactoryBot.define do
  factory :user do
    username { "太郎" }
    sequence(:email) { |n| "test#{n}@example.com" }
    password { "password" }
    avatar do
      Rack::Test::UploadedFile.new(Rails.root.join('spec/factories/test_avatar.jpg'), 'image/jpeg')
    end
    introduction { "テスト" }
    favorite { nil }
    admin { false }

    factory :admin_user do
      admin { true }
    end

    trait :with_post do
      after(:create) { |user| create(:post, user: user) }
    end
  end
end
