FactoryBot.define do
  factory :post do
    title { "テスト" }
    description { "テスト" }
    recomend { "ハンバーガー" }
    picture do
      Rack::Test::UploadedFile.new(Rails.root.join('spec/factories/test.jpg'), 'image/jpeg')
    end
    user
    shop
  end
end
