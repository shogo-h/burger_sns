FactoryBot.define do
  factory :comment do
    content { "テスト" }
    post
    user
  end
end
