require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe "Comment" do
    it "ファクトリが有効であること" do
      comment = build(:comment)
      expect(comment).to be_valid
    end

    it "ユーザーが関連していない場合、無効であること" do
      comment = build(:comment, user_id: nil)
      comment.valid?
      expect(comment.errors[:user]).to include('を入力してください')
    end

    it "レビューが関連していない場合、無効であること" do
      comment = build(:comment, post_id: nil)
      comment.valid?
      expect(comment.errors[:post]).to include('を入力してください')
    end

    it "内容が空の場合、無効であること" do
      comment = build(:comment, content: nil)
      comment.valid?
      expect(comment.errors[:content]).to include('を入力してください')
    end

    it "内容が140文字を超えた場合、無効であること" do
      comment = build(:comment, content: "a" * 141)
      comment.valid?
      expect(comment.errors[:content]).to include('は140文字以内で入力してください')
    end
  end
end
