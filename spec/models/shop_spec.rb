require 'rails_helper'

RSpec.describe Shop, type: :model do
  describe "Shop" do
    it "ファクトリが有効であること" do
      shop = build(:shop)
      expect(shop).to be_valid
    end

    it '店名,最寄駅があれば有効であること' do
      shop = Shop.new(
        name: "ハンバーガー屋",
        station: "渋谷駅"
      )
      expect(shop).to be_valid
    end

    it '店名がなければ、無効であること' do
      shop = build(:shop, name: nil)
      shop.valid?
      expect(shop.errors[:name]).to include("を入力してください")
    end

    it '最寄駅がなければ、無効であること' do
      shop = build(:shop, station: nil)
      shop.valid?
      expect(shop.errors[:station]).to include("を入力してください")
    end

    it '最寄駅の文字数が30文字を超えた場合、無効であること' do
      shop = build(:shop, station: "あ" * 31)
      shop.valid?
      expect(shop.errors[:station]).to include("は30文字以内で入力してください")
    end

    it '最寄駅の文字数が30文字ならば、有効であること' do
      shop = build(:shop, station: "あ" * 30)
      expect(shop).to be_valid
    end

    describe "お店に関連しているブックマーク数をカウントして返す" do
      let!(:shop) { create(:shop) }
      let!(:user) { create(:user) }

      it "ブックマーク数が1ならば、1を返す" do
        Bookmark.create(shop_id: shop.id, user_id: user.id)
        expect(shop.bookmarks_count).to eq 1
      end

      it "ブックマークされていなければ、0を返す" do
        expect(shop.bookmarks_count).to eq 0
      end
    end

    describe "newest scope" do
      let!(:new_shop) { create(:shop, created_at: Time.now) }
      let!(:old_shop) { create(:shop, created_at: Time.now.yesterday) }
      let!(:oldest_shop) { create(:shop, created_at: Time.now.ago(2.days)) }

      it "お店データをcreated_atが新しい順に取得して返す" do
        expect(Shop.newest).to match [new_shop, old_shop, oldest_shop]
      end
    end
  end
end
