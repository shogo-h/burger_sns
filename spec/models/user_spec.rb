require 'rails_helper'

RSpec.describe User, type: :model do
  describe "User" do
    it "ファクトリが有効であること" do
      user = build(:user)
      expect(user).to be_valid
    end

    it "アカウント名がなければ無効であること" do
      user = build(:user, username: nil)
      user.valid?
      expect(user.errors[:username]).to include("を入力してください")
    end

    it "メールアドレスがなければ無効であること" do
      user = build(:user, email: nil)
      user.valid?
      expect(user.errors[:email]).to include("を入力してください")
    end

    it "パスワードがなければ無効であること" do
      user = build(:user, password: nil)
      user.valid?
      expect(user.errors[:password]).to include("を入力してください")
    end

    it "メールアドレスが重複していれば、無効であること" do
      user = create(:user)
      other_user = build(:user, email: user.email)
      other_user.valid?
      expect(other_user.errors[:email]).to include("はすでに存在します")
    end

    it '自己紹介の文字数が300文字を超えた場合、無効であること' do
      user = build(:user, introduction: "あ" * 301)
      user.valid?
      expect(user.errors[:introduction]).to include("は300文字以内で入力してください")
    end

    it '自己紹介の文字数が300文字ならば、有効であること' do
      user = build(:user, introduction: "あ" * 300)
      user.valid?
      expect(user).to be_valid
    end

    describe 'favorite_shop_name' do
      it "お気に入りのお店を登録していれば、店名を返すこと" do
        shop = create(:shop, name: "立石バーガー")
        user = create(:user, favorite: shop.id)
        expect(user.favorite_shop_name).to eq "立石バーガー"
      end

      it "お気に入りのお店を登録していなければ、-を返すこと" do
        user = create(:user, favorite: nil)
        expect(user.favorite_shop_name).to eq "-"
      end
    end

    describe 'ユーザーの投稿に関連する、いいねの数を返す' do
      let(:user) { create(:user) }

      it "ユーザーのレビューにいいねが1つあれば、1を返すこと" do
        shop = create(:shop)
        post = create(:post, shop: shop, user: user)
        Like.create(post: post, user: user)
        expect(user.get_likes).to eq 1
      end

      it "ユーザーのレビューにいいねがなければ、0を返すこと" do
        expect(user.get_likes).to eq 0
      end
    end

    describe 'ユーザーをフォロー/アンフォローする' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user, email: 'other@example.com') }

      before do
        user.follow(other_user)
      end

      it 'userがother_userをフォローしていること(following)' do
        expect(user.following).to include other_user
      end

      it 'userがother_userをフォローしていること(following?)' do
        expect(user.following?(other_user)).to be_truthy
      end

      it 'userがother_userをアンフォローしていること' do
        user.unfollow(other_user)
        expect(user.active_relationships.find_by(followed_id: other_user.id)).to be_falsey
      end
    end
  end
end
