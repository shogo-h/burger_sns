require 'rails_helper'

RSpec.describe Relationship, type: :model do
  describe "Relationship" do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user) }

    it 'フォローとフォロワーがあるとき、関係が有効であること' do
      relationship = Relationship.new(follower_id: user.id, followed_id: other_user.id)
      expect(relationship).to be_valid
    end

    it 'フォローが存在しないとき、関係が無効であること' do
      relationship = Relationship.new(follower_id: user.id, followed_id: nil)
      relationship.valid?
      expect(relationship.errors[:followed_id]).to include('を入力してください')
    end

    it 'フォロワーが存在しないとき、関係が無効であること' do
      relationship = Relationship.new(follower_id: nil, followed_id: other_user.id)
      relationship.valid?
      expect(relationship.errors[:follower_id]).to include('を入力してください')
    end
  end
end
