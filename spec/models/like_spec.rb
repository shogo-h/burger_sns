require 'rails_helper'

RSpec.describe Like, type: :model do
  describe 'Like' do
    it "ユーザーとレビューと関連していれば、有効であること" do
      post = create(:post)
      user = create(:user)
      like = Like.create(post_id: post.id, user_id: user.id)
      expect(like).to be_valid
    end

    it "ユーザーが関連していない場合、無効であること" do
      post = create(:post)
      like = Like.create(post_id: post.id, user_id: nil)
      like.valid?
      expect(like.errors[:user]).to include('を入力してください')
    end

    it "レビューが関連していない場合、無効であること" do
      user = create(:user)
      like = Like.create(post_id: nil, user_id: user.id)
      like.valid?
      expect(like.errors[:post]).to include('を入力してください')
    end

    it "ユーザーとレビューの組み合わせが重複すれば、無効であること" do
      post = create(:post)
      user = create(:user)
      Like.create(post_id: post.id, user_id: user.id)
      like = Like.new(post_id: post.id, user_id: user.id)
      like.valid?
      expect(like.errors[:post_id]).to include('はすでに存在します')
    end
  end
end
