require 'rails_helper'

RSpec.describe Bookmark, type: :model do
  describe 'Bookmark' do
    it "ファクトリが有効であること" do
      bookmark = build(:bookmark)
      expect(bookmark).to be_valid
    end

    it "ユーザーが関連していない場合、無効であること" do
      bookmark = build(:bookmark, user_id: nil)
      bookmark.valid?
      expect(bookmark.errors[:user]).to include('を入力してください')
    end

    it "ショップが関連していない場合、無効であること" do
      bookmark = build(:bookmark, shop_id: nil)
      bookmark.valid?
      expect(bookmark.errors[:shop]).to include('を入力してください')
    end

    it "ユーザーとショップの組み合わせが重複すれば、無効であること" do
      user = create(:user)
      shop = create(:shop)
      create(:bookmark, user_id: user.id, shop_id: shop.id)
      bookmark = build(:bookmark, user_id: user.id, shop_id: shop.id)
      bookmark.valid?
      expect(bookmark.errors[:shop_id]).to include('はすでに存在します')
    end
  end
end
