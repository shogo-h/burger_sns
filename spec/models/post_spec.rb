require 'rails_helper'

RSpec.describe Post, type: :model do
  describe 'Post' do
    it 'ファクトリが有効であること' do
      post = build(:post)
      expect(post).to be_valid
    end

    it 'タイトルがなければ、無効であること' do
      post = build(:post, title: nil)
      post.valid?
      expect(post.errors[:title]).to include("を入力してください")
    end

    it '内容がなければ、無効であること' do
      post = build(:post, description: nil)
      post.valid?
      expect(post.errors[:description]).to include("を入力してください")
    end

    it 'タイトルの文字数が30文字を超えた場合、無効であること' do
      post = build(:post, title: 'a' * 31)
      post.valid?
      expect(post.errors[:title]).to include("は30文字以内で入力してください")
    end

    it 'おすすめメニューの文字数が30文字を超えた場合、無効であること' do
      post = build(:post, recomend: 'a' * 31)
      post.valid?
      expect(post.errors[:recomend]).to include("は30文字以内で入力してください")
    end
  end
end
