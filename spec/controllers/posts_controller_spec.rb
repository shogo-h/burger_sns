require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  describe '#index' do
    let!(:shop) { create(:shop) }

    before do
      get :index, params: { shop_id: shop }
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '200レスポンスを返すこと' do
      expect(response).to have_http_status 200
    end
  end

  describe '#show' do
    let!(:post) { create(:post) }

    before do
      get :show, params: { id: post.id }
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '200レスポンスを返すこと' do
      expect(response).to have_http_status 200
    end
  end

  describe '#new' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:shop) { create(:shop) }

      before do
        sign_in user
        get :new, params: { shop_id: shop }
      end

      it '正常にレスポンスを返すこと' do
        expect(response).to be_successful
      end

      it '200レスポンスを返すこと' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしていない場合' do
      let!(:shop) { create(:shop) }

      before do
        get :new, params: { shop_id: shop }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#create' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:shop) { create(:shop) }

      before do
        sign_in user
      end

      it 'レビューを登録できる' do
        post_params = FactoryBot.attributes_for(:post)
        expect { post :create, params: { post: post_params, shop_id: shop } }.
          to change { user.posts.count }.by(1)
      end

      it 'レビューを登録後、詳細ページにリダイレクトする' do
        post_params = FactoryBot.attributes_for(:post)
        post :create, params: { post: post_params, shop_id: shop }
        post = Post.last
        expect(response).to redirect_to post_path(post)
      end

      it 'レビューの登録に失敗すると、新規登録ページをレンダリングする' do
        post_params = FactoryBot.attributes_for(:post, title: nil)
        post :create, params: { post: post_params, shop_id: shop }
        expect(response).to render_template(:new)
      end
    end

    context 'ログインしていない場合' do
      let!(:shop) { create(:shop) }

      before do
        post_params = FactoryBot.attributes_for(:post)
        post :create, params: { post: post_params, shop_id: shop }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#edit' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:post) { create(:post, user_id: user.id) }

      before do
        sign_in user
        get :edit, params: { id: post.id }
      end

      it '正常にレスポンスを返すこと' do
        expect(response).to be_successful
      end

      it '200レスポンスを返すこと' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしており、他のユーザーのレビューを編集する場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user) }
      let!(:post) { create(:post, user_id: user.id) }

      before do
        sign_in other_user
        get :edit, params: { id: post.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'トップページにリダイレクトする' do
        expect(response).to redirect_to root_path
      end
    end

    context 'ログインしていない場合' do
      let!(:post) { create(:post) }

      before do
        get :edit, params: { id: post.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#update' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:post) { create(:post, user_id: user.id) }

      before do
        sign_in user
      end

      it 'レビューを更新できる' do
        post_params = FactoryBot.attributes_for(:post, title: '更新')
        patch :update, params: { id: post.id, post: post_params }
        expect(post.reload.title).to eq '更新'
      end

      it 'レビューを更新後、詳細ページにリダイレクトする' do
        post_params = FactoryBot.attributes_for(:post, title: '更新')
        patch :update, params: { id: post.id, post: post_params }
        expect(response).to redirect_to post_path(post)
      end

      it 'レビューの更新に失敗すると、編集ページをレンダリングする' do
        post_params = FactoryBot.attributes_for(:post, title: nil)
        patch :update, params: { id: post.id, post: post_params }
        expect(response).to render_template(:edit)
      end
    end

    context 'ログインしており、他のユーザーのレビューを更新する場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user) }
      let!(:post) { create(:post, user_id: user.id) }

      before do
        sign_in other_user
        post_params = FactoryBot.attributes_for(:post)
        patch :update, params: { id: post.id, post: post_params }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'トップページにリダイレクトする' do
        expect(response).to redirect_to root_path
      end
    end

    context 'ログインしていない場合' do
      let!(:post) { create(:post) }

      before do
        post_params = FactoryBot.attributes_for(:post)
        patch :update, params: { id: post.id, post: post_params }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#destroy' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:post) { create(:post, user_id: user.id) }

      before do
        sign_in user
      end

      it 'レビューを削除できる' do
        expect { delete :destroy, params: { id: post.id } }.to change { user.posts.count }.by(-1)
      end

      it 'レビュー削除後、レビュー一覧ページにリダイレクトする' do
        delete :destroy, params: { id: post.id }
        expect(response).to redirect_to root_path
      end
    end

    context 'ログインしており、他のユーザーのレビューを削除する場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user) }
      let!(:post) { create(:post, user_id: user.id) }

      before do
        sign_in other_user
        delete :destroy, params: { id: post.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'トップページにリダイレクトする' do
        expect(response).to redirect_to root_path
      end
    end

    context 'ログインしていない場合' do
      let!(:post) { create(:post) }

      before do
        delete :destroy, params: { id: post.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#timeline' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }

      before do
        sign_in user
        get :timeline
      end

      it '正常にレスポンスを返すこと' do
        expect(response).to be_successful
      end

      it '200レスポンスを返すこと' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしていない場合' do
      before do
        get :timeline
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
