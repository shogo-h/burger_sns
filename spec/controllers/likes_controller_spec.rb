require 'rails_helper'

RSpec.describe LikesController, type: :controller do
  describe '#create' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:one_post) { create(:post) }

      before do
        sign_in user
      end

      it 'いいねできる' do
        like_params = FactoryBot.attributes_for(:like)
        expect { post :create, params: { like: like_params, post_id: one_post.id } }.
          to change { one_post.likes.count }.by(1)
      end

      it 'いいね後、レビュー詳細ページにリダイレクトする' do
        like_params = FactoryBot.attributes_for(:like)
        post :create, params: { like: like_params, post_id: one_post.id }
        expect(response).to redirect_to post_path(one_post)
      end
    end

    context 'ログインしていない場合' do
      let!(:one_post) { create(:post) }

      before do
        like_params = FactoryBot.attributes_for(:like)
        post :create, params: { like: like_params, post_id: one_post.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#destroy' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:one_post) { create(:post) }
      let!(:like) { create(:like, user_id: user.id, post_id: one_post.id) }

      before do
        sign_in user
      end

      it 'いいねを削除できる' do
        expect { delete :destroy, params: { id: like.id, post_id: one_post.id } }.
          to change { one_post.likes.count }.by(-1)
      end

      it 'いいね削除後、レビュー詳細ページにリダイレクトする' do
        delete :destroy, params: { id: like.id, post_id: one_post.id }
        expect(response).to redirect_to post_path(like.post_id)
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }
      let!(:one_post) { create(:post) }
      let!(:like) { create(:like, user_id: user.id, post_id: one_post.id) }

      before do
        delete :destroy, params: { id: like.id, post_id: one_post.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
