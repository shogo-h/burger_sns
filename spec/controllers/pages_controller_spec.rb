require 'rails_helper'

RSpec.describe PagesController, type: :controller do
  describe '#index' do
    before do
      get :index
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '200レスポンスを返すこと' do
      expect(response).to have_http_status 200
    end
  end
end
