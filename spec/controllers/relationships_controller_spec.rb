require 'rails_helper'

RSpec.describe RelationshipsController, type: :controller do
  describe '#create' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user) }

      before do
        sign_in user
      end

      it 'フォローできる' do
        expect { post :create, params: { followed_id: other_user.id } }.
          to change { user.following.count }.by(1)
      end

      it 'フォロー後、ユーザー詳細ページにリダイレクトする' do
        post :create, params: { followed_id: other_user.id }
        expect(response).to redirect_to user_path(other_user)
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user) }

      before do
        post :create, params: { followed_id: other_user.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#destroy' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user) }

      before do
        sign_in user
        user.follow(other_user)
        @followed_id = user.active_relationships.find_by(followed_id: other_user.id)
      end

      it 'アンフォローできる' do
        expect { delete :destroy, params: { id: @followed_id } }.
          to change { user.following.count }.by(-1)
      end

      it 'アンフォロー後、ユーザー詳細ページにリダイレクトする' do
        delete :destroy, params: { id: @followed_id }
        expect(response).to redirect_to user_path(other_user)
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user) }

      before do
        Relationship.create(follower_id: user.id, followed_id: other_user.id)
        followed_id = user.active_relationships.find_by(followed_id: other_user.id)
        delete :destroy, params: { id: followed_id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
