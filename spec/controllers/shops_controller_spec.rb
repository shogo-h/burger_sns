require 'rails_helper'

RSpec.describe ShopsController, type: :controller do
  describe '#index' do
    before do
      get :index
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '200レスポンスを返すこと' do
      expect(response).to have_http_status 200
    end
  end

  describe '#show' do
    let!(:shop) { create(:shop) }

    before do
      get :show, params: { id: shop.id }
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '200レスポンスを返すこと' do
      expect(response).to have_http_status 200
    end
  end

  describe '#new' do
    let!(:user) { create(:user) }

    context 'ログインしている場合' do
      before do
        sign_in user
        get :new
      end

      it '正常にレスポンスを返すこと' do
        expect(response).to be_successful
      end

      it '200レスポンスを返すこと' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしていない場合' do
      before do
        get :new
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#create' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }

      before do
        sign_in user
      end

      it 'お店を登録できる' do
        shop_params = FactoryBot.attributes_for(:shop)
        expect { post :create, params: { shop: shop_params } }.to change(Shop, :count).by(1)
      end

      it 'お店を登録後、詳細ページにリダイレクトする' do
        shop_params = FactoryBot.attributes_for(:shop)
        post :create, params: { shop: shop_params }
        shop = Shop.last
        expect(response).to redirect_to shop_path(shop)
      end

      it 'お店の登録に失敗すると、新規登録ページをレンダリングする' do
        shop_params = FactoryBot.attributes_for(:shop, name: nil)
        post :create, params: { shop: shop_params }
        expect(response).to render_template(:new)
      end
    end

    context 'ログインしていない場合' do
      before do
        shop_params = FactoryBot.attributes_for(:shop)
        post :create, params: { shop: shop_params }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#edit' do
    let!(:shop) { create(:shop) }

    context 'ログインしている場合' do
      let!(:user) { create(:user) }

      before do
        sign_in user
        get :edit, params: { id: shop.id }
      end

      it '正常にレスポンスを返すこと' do
        expect(response).to be_successful
      end

      it '200レスポンスを返すこと' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしていない場合' do
      before do
        get :edit, params: { id: shop.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#update' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:shop) { create(:shop) }

      before do
        sign_in user
      end

      it 'お店を更新できる' do
        shop_params = FactoryBot.attributes_for(:shop, name: 'テスト')
        patch :update, params: { id: shop.id, shop: shop_params }
        expect(shop.reload.name).to eq 'テスト'
      end

      it 'お店を更新後、詳細ページにリダイレクトする' do
        shop_params = FactoryBot.attributes_for(:shop)
        patch :update, params: { id: shop.id, shop: shop_params }
        expect(response).to redirect_to shop_path(shop)
      end

      it 'お店の更新に失敗すると、編集ページをレンダリングする' do
        shop_params = FactoryBot.attributes_for(:shop, name: nil)
        patch :update, params: { id: shop.id, shop: shop_params }
        expect(response).to render_template(:edit)
      end
    end

    context 'ログインしていない場合' do
      let!(:shop) { create(:shop) }

      before do
        shop_params = FactoryBot.attributes_for(:shop)
        patch :update, params: { id: shop.id, shop: shop_params }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#destroy' do
    context '管理権限を持ったユーザーでログインしている場合' do
      let!(:user) { create(:admin_user) }
      let!(:shop) { create(:shop) }

      before do
        sign_in user
      end

      it 'お店を削除できる' do
        expect { delete :destroy, params: { id: shop.id } }.to change(Shop, :count).by(-1)
      end

      it 'お店削除後、お店一覧ページにリダイレクトする' do
        delete :destroy, params: { id: shop.id }
        expect(response).to redirect_to shops_path
      end
    end

    context '一般ユーザーでログインしている場合' do
      let!(:user) { create(:user) }
      let!(:shop) { create(:shop) }

      before do
        sign_in user
        delete :destroy, params: { id: shop.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'トップページにリダイレクトする' do
        expect(response).to redirect_to root_path
      end
    end

    context 'ログインしていない場合' do
      let!(:shop) { create(:shop) }

      before do
        delete :destroy, params: { id: shop.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
