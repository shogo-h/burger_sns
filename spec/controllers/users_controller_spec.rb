require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe '#index' do
    before do
      get :index
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '200レスポンスを返すこと' do
      expect(response).to have_http_status 200
    end
  end

  describe '#show' do
    let!(:shop) { create(:shop) }
    let!(:user) { create(:user, favorite: shop.id) }

    before do
      get :show, params: { id: user.id }
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '200レスポンスを返すこと' do
      expect(response).to have_http_status 200
    end
  end

  describe '#bookmark' do
    let!(:shop) { create(:shop) }
    let!(:user) { create(:user, favorite: shop.id) }

    before do
      get :bookmark, params: { id: user.id }
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '200レスポンスを返すこと' do
      expect(response).to have_http_status 200
    end
  end

  describe '#following' do
    let!(:shop) { create(:shop) }
    let!(:user) { create(:user, favorite: shop.id) }

    before do
      get :following, params: { id: user.id }
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '200レスポンスを返すこと' do
      expect(response).to have_http_status 200
    end
  end

  describe '#followers' do
    let!(:shop) { create(:shop) }
    let!(:user) { create(:user, favorite: shop.id) }

    before do
      get :followers, params: { id: user.id }
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '200レスポンスを返すこと' do
      expect(response).to have_http_status 200
    end
  end
end
