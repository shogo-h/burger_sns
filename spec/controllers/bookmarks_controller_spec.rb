require 'rails_helper'

RSpec.describe BookmarksController, type: :controller do
  describe '#create' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:shop) { create(:shop) }

      before do
        sign_in user
      end

      it '行きたい店に登録できる' do
        bookmark_params = FactoryBot.attributes_for(:bookmark)
        expect { post :create, params: { bookmark: bookmark_params, shop_id: shop.id } }.
          to change { shop.bookmarks.count }.by(1)
      end

      it '行きたい店に登録後、お店詳細ページにリダイレクトする' do
        bookmark_params = FactoryBot.attributes_for(:bookmark)
        post :create, params: { bookmark: bookmark_params, shop_id: shop.id }
        expect(response).to redirect_to shop_path(shop)
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }
      let!(:shop) { create(:shop) }

      before do
        bookmark_params = FactoryBot.attributes_for(:bookmark)
        post :create, params: { bookmark: bookmark_params, shop_id: shop.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#destroy' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:shop) { create(:shop) }
      let!(:bookmark) { create(:bookmark, user_id: user.id, shop_id: shop.id) }

      before do
        sign_in user
      end

      it '行きたい店を削除できる' do
        expect { delete :destroy, params: { id: bookmark.id, shop_id: shop.id } }.
          to change { shop.bookmarks.count }.by(-1)
      end

      it '行きたい店を削除後、お店詳細ページにリダイレクトする' do
        delete :destroy, params: { id: bookmark.id, shop_id: shop.id }
        expect(response).to redirect_to shop_path(bookmark.shop_id)
      end
    end

    context 'ログインしていない場合' do
      let!(:shop) { create(:shop) }
      let!(:bookmark) { create(:bookmark, shop_id: shop.id) }

      before do
        delete :destroy, params: { id: bookmark.id, shop_id: shop.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
