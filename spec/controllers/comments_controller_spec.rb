require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  describe '#create' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:one_post) { create(:post, user_id: user.id) }

      before do
        sign_in user
      end

      it 'コメントを登録できる' do
        comment_params = FactoryBot.attributes_for(:comment)
        expect { post :create, params: { comment: comment_params, post_id: one_post.id } }.
          to change { user.comments.count }.by(1)
      end

      it 'コメントを登録後、レビュー詳細ページにリダイレクトする' do
        comment_params = FactoryBot.attributes_for(:comment)
        post :create, params: { comment: comment_params, post_id: one_post.id }
        expect(response).to redirect_to post_path(one_post)
      end

      it 'コメントの登録に失敗すると、レビュー詳細ページをレンダリングする' do
        comment_params = FactoryBot.attributes_for(:comment, content: nil)
        post :create, params: { comment: comment_params, post_id: one_post.id }
        expect(response).to render_template("posts/show")
      end
    end

    context 'ログインしていない場合' do
      let!(:one_post) { create(:post) }

      before do
        comment_params = FactoryBot.attributes_for(:comment)
        post :create, params: { comment: comment_params, post_id: one_post.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe '#destroy' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:one_post) { create(:post) }
      let!(:comment) { create(:comment, user_id: user.id, post_id: one_post.id) }

      before do
        sign_in user
      end

      it 'コメントを削除できる' do
        expect { delete :destroy, params: { id: comment.id, post_id: one_post.id } }.
          to change { user.comments.count }.by(-1)
      end

      it 'コメント削除後、レビュー詳細ページにリダイレクトする' do
        delete :destroy, params: { id: comment.id, post_id: one_post.id }
        expect(response).to redirect_to post_path(comment.post_id)
      end
    end

    context 'ログインしていない場合' do
      let!(:one_post) { create(:post) }
      let(:comment) { create(:comment, post_id: one_post.id) }

      before do
        delete :destroy, params: { id: comment.id, post_id: one_post.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
