# This file should contain all the record creation needed to seed the database
#   with its default values.
# The data can then be loaded with the rails db:seed command
#   (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
30.times do
  Shop.create!(
    name: Faker::Dog.bread,
    menu: "ハンバーガー",
    hour: "12:00-22:00",
    holiday: "日曜日",
    url: "https://example.com",
    phone: Faker::PhoneNumber.phone_number,
    address: "#{Faker::Address.state +
                Faker::Address.city +
                Faker::Name.last_name +
                Faker::Address.state_abbr}
                -#{Faker::Address.state_abbr}
                -#{Faker::Address.state_abbr}",
    station: "原宿駅"
  )
end

shops = Shop.pluck(:id)

User.create!(
  username: "管理人",
  email: "admin@example.com",
  password: "password",
  introduction: "よろしくおねがいします！",
  favorite: shops.sample,
  admin: "true"
)

30.times do
  User.create!(
    username: Faker::Name.first_name,
    email: Faker::Internet.unique.email,
    password: "password",
    introduction: "よろしくおねがいします！",
    favorite: shops.sample
  )
end

users = User.first(4)
title = "ダミーテキスト。"
description = "親譲りの無鉄砲で小供の時から損ばかりしている。
  小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。
  なぜそんな無闇をしたと聞く人があるかも知れぬ。別段深い理由でもない。
  新築の二階から首を出していたら、同級生の一人が冗談に、いくら威張っても、そこから飛び降りる事は出来まい。
  弱虫やーい。と囃したからである。小使に負ぶさって帰って来た時、おやじが大きな眼をして二階ぐらいから飛び降りて腰"
recomend = "ハンバーガー"

30.times do
  users.each do |user|
    user.posts.create!(
      title: title,
      description: description,
      recomend: recomend,
      shop_id: shops.sample
    )
  end
end

posts = Post.pluck(:id)

30.times do
  users.each do |user|
    user.comments.create!(content: Faker::Lorem.word, post_id: posts.sample)
  end
end

users = User.first(20)
post = users[0].posts.first
users.each do |user|
  Like.create!(user_id: user.id, post_id: post.id)
  Bookmark.create!(user_id: user.id, shop_id: shops[0])
end

users = User.first(15)
post = users[1].posts.first
users.each do |user|
  Like.create!(user_id: user.id, post_id: post.id)
  Bookmark.create!(user_id: user.id, shop_id: shops[1])
end

users = User.first(10)
post = users[2].posts.first
users.each do |user|
  Like.create!(user_id: user.id, post_id: post.id)
  Bookmark.create!(user_id: user.id, shop_id: shops[2])
end

users = User.first(5)
post = users[3].posts.first
users.each do |user|
  Like.create!(user_id: user.id, post_id: post.id)
  Bookmark.create!(user_id: user.id, shop_id: shops[3])
end

user = User.first
post = Post.last

Like.create!(user_id: user.id, post_id: post.id)
Bookmark.create!(user_id: user.id, shop_id: shops[4])

users = User.all
user = users.first
following = users[2..30]
followers = users[2..30]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
