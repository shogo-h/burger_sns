class ChangeStationToShops < ActiveRecord::Migration[5.2]
  def up
    change_column :shops, :station, :string, null: false
  end

  def down
    change_column :shops, :station, :string, null: true
  end
end
