class AddRecomendToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :recomend, :string, limit: 30
  end
end
