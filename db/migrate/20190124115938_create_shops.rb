class CreateShops < ActiveRecord::Migration[5.2]
  def change
    create_table :shops do |t|
      t.string :name, null: false
      t.text :menu
      t.string :hour
      t.string :holiday
      t.string :address
      t.string :phone
      t.string :url
      t.timestamps
    end
  end
end
