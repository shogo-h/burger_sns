class AddPostsCountToShops < ActiveRecord::Migration[5.2]
  def self.up
    add_column :shops, :posts_count, :integer, null: false, default: 0
  end

  def self.down
    remove_column :shops, :posts_count
  end
end
