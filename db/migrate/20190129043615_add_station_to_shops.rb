class AddStationToShops < ActiveRecord::Migration[5.2]
  def change
    add_column :shops, :station, :string, limit: 30
  end
end
