# ハンバーガー専門の口コミSNSサイト
オリジナルサービスとして、ハンバーガー専門の口コミSNSサイトを制作しました。（ハンバーガーが好きなため）    

## 主な実装内容
* ユーザー認証（gem: devise）
* 管理者権限・管理画面（gem: rails_admin, cancancan）
* お店の登録・編集・更新・削除
* レビューの登録・編集・更新・削除
* コメント機能
* フォロー機能
* いいね機能
* ブックマーク機能
* お店、ユーザーの検索機能（gem: ransack使用）
* パンくずリスト生成（gem: gretel）
* ページネーション機能（gem: kaminari）
* 画像アップロード機能（gem: carrierwave, mini_magick）※本番環境ではAmazon S3を利用
* Rspec導入・・・controller spec, model spec, feature specを作成
* RuboCop airbnb 導入
* bullet,counter_culture 導入（N+1問題の対策）
* Like,Bookmark,FollowをAjax対応

<https://burger-mania-2019.herokuapp.com/>  
