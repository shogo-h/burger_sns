crumb :root do
  link "Home", root_path
end

crumb :shops do
  link "お店を探す", shops_path
end

crumb :shop_new do
  link "お店登録", new_shop_path
end

crumb :shop do |shop|
  link shop.name, shop_path(shop)
  parent :shops
end

crumb :shop_edit do |shop|
  link "お店編集", edit_shop_path(shop)
  parent :shop, shop
end

crumb :posts do |shop|
  link "レビュー一覧", shop_posts_path(shop)
  parent :shop, shop
end

crumb :post do |shop, post|
  link "レビュー詳細", post_path(post)
  parent :posts, shop
end

crumb :post_new do |shop|
  link "レビュー投稿", new_shop_post_path(shop)
  parent :shop, shop
end

crumb :post_edit do |shop, post|
  link "レビュー編集", edit_post_path(post)
  parent :post, shop, post
end

crumb :signup do
  link "新規登録", new_user_registration_path
end

crumb :login do
  link "ログイン", new_user_session_path
end

crumb :users do
  link "ユーザーを探す", users_path
end

crumb :user do |user|
  link "#{user.username}さんのプロフィール", user_path(user)
  parent :users
end

crumb :user_edit do |user|
  link "ユーザー編集", edit_user_registration_path
  parent :user, user
end

crumb :user_follow do |user|
  link "フォロー", following_user_path(user)
  parent :user, user
end

crumb :user_follower do |user|
  link "フォロワー", followers_user_path(user)
  parent :user, user
end

crumb :user_bookmark do |user|
  link "行きたいお店", bookmark_user_path(user)
  parent :user, user
end

crumb :timeline do
  link "タイムライン", timeline_path
end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).
