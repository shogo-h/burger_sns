Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root 'pages#index'
  get  '/timeline', to: 'posts#timeline'
  devise_for  :users, controllers: { registrations: 'users/registrations' }
  resources   :users, only: [:index, :show] do
    member do
      get :bookmark, :following, :followers
    end
  end
  resources :shops do
    resources   :posts, only: [:index, :new, :create]
    resources   :bookmarks, only: [:create, :destroy]
  end
  resources   :posts, except: [:index, :new, :create] do
    resources :comments, only: [:create, :destroy]
  end
  resources   :likes, only: [:create, :destroy]
  resources   :relationships, only: [:create, :destroy]
end
